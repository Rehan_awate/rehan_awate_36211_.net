﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01demo
{
    class Program
    {
        static void Main(string[] args)
        {

            //by object
        //    Object[] obj = new Object[5];

        //    Emp e1 = new Emp();

        //    e1.No = 1;
        //    e1.Name = "rehan";
        //    e1.Dname = "ece";

        //    Emp e2 = new Emp();
        //    e2.No = 2;
        //    e2.Name = "yogesh";
        //    e2.Dname = "cs";

        //    Emp e3 = new Emp();
        //    e3.No = 3;
        //    e3.Name = "renu";
        //    e3.Dname = "eee";

        //    Customer c1 = new Customer();
        //    c1.No = 1;
        //    c1.Name = "onkar";
        //    c1.Orderdetails = "pizza";

        //    Customer c2 = new Customer();
        //    c2.No = 2;
        //    c2.Name = "omkar1";
        //    c2.Orderdetails = "chese";

        //    obj[0] = e1;
        //    obj[1] = e2;
        //    obj[2] = e3;
        //    obj[3] = c1;
        //    obj[4] = c2;





        //    for (int i = 0; i < obj.Length; i++)
        //    {
        //        if (obj[i] != null)
        //        {
        //            if (obj[i] is Emp)
        //            {
        //                Emp e = (Emp)obj[i];
        //                Console.WriteLine(e.GetDetails());

        //            }
        //            else
        //            {
        //                Customer c = (Customer)obj[i];
        //                Console.WriteLine(c.GetDetails());

        //            }
        //        }
        //    }
        //}
    }

    class Emp
    {
        private string _Dname;
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }


        public string Dname
        {
            get { return _Dname; }
            set { _Dname = value; }
        }

        public string GetDetails()
        {
            return " No " + this.No + " Name " + this.Name + " dname " + this._Dname;
        }
    }
    class Customer
    {
        private string _OrderDetails;

        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string Orderdetails
        {
            get { return _OrderDetails; }
            set { _OrderDetails = value; }
        }

        public string GetDetails()
        {
            return " No " + this.No + " Name " + this.Name + "  orderDetails  " + this.Orderdetails;
        }
    }














        #region Ans by inheritance
        //        Emp e1 = new Emp();
        //        e1.No = 1;
        //        e1.Name = "rehan";
        //        e1.Dname = "ece";

        //        Emp e2 = new Emp();
        //        e2.No = 2;
        //        e2.Name = "yogesh";
        //        e2.Dname = "cs";

        //        Emp e3 = new Emp();
        //        e3.No = 3;
        //        e3.Name = "renu";
        //        e3.Dname = "eee";

        //        Customer c1 = new Customer();
        //        c1.No = 1;
        //        c1.Name = "onkar";
        //        c1.Orderdetails = "pizza";

        //        Customer c2 = new Customer();
        //        c2.No = 2;
        //        c2.Name = "omkar";
        //        c2.Orderdetails = "chese";

        //        Person[] p1 = new Person[5];

        //        p1[0] = e1;
        //        p1[1] = e2;
        //        p1[2] = e3;
        //        p1[3] = c1;
        //        p1[4] = c2;

        //        for (int i = 0; i < p1.Length; i++)
        //        {
        //            if (p1[i] != null)
        //            {
        //                Console.WriteLine(p1[i].GetDetails());
        //            }
        //        }


        //    }
        //}
        //class Person
        //{
        //    private int _No;
        //    private string _Name;

        //    public string Name
        //    {
        //        get { return _Name; }
        //        set { _Name = value; }
        //    }

        //    public int No
        //    {
        //        get { return _No; }
        //        set { _No = value; }
        //    }

        //    public virtual string GetDetails()
        //    {
        //        return " no " + this.No +  " Name " + this.Name;
        //    }
        //}

        //class Emp: Person
        //{
        //    private string _Dname;

        //    public string Dname
        //    {
        //        get { return _Dname; }
        //        set { _Dname = value; }
        //    }

        //    public override string GetDetails()
        //    {
        //        return base.GetDetails() + " dname " + this._Dname ;
        //    }
        //}
        //class Customer : Person
        //{
        //    private string _OrderDetails;

        //    public string Orderdetails
        //    {
        //        get { return _OrderDetails; }
        //        set { _OrderDetails = value; }
        //    }

        //    public override string GetDetails()
        //    {
        //        return base.GetDetails() + "  orderDetails  " + this.Orderdetails;
        //    }

        //} 
        #endregion

    
}
