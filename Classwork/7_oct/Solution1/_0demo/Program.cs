﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0demo
{
    class Program
    {
        static void Main(string[] args)
        {

            #region simple typecast
            //  Emp e1 = new Emp();
            //  e1.Name = "rehan";
            //  e1.No = 1;
            //  Object obj = new Object();

            //  obj = 2;
            //  obj = "reh";
            //  obj = e1;

            //  if (obj is int)
            //  {
            //      int p = Convert.ToInt32(obj);
            //      Console.WriteLine(p);
            //  }
            // else if (obj is string)
            //  {
            //      string s = Convert.ToString(obj);
            //      Console.WriteLine(s);
            //  }
            //else  if (obj is Emp)
            //  {
            //      Emp e = (Emp)obj;
            //      Console.WriteLine(e.getDetails());
            //  } 
            #endregion

            #region Simple array
            //string [] arr = new string [4];

            // string[] arr1 = new string[] { "re","ha","n" };
            // arr[0] = "r";
            // arr[1] = "e";
            // arr[2] = "h";

            // for (int i = 0; i < arr.Length; i++)
            // {
            //     Console.WriteLine(arr[i]);
            //     Console.WriteLine(arr1[i]);
            // } 
            #endregion

            #region Emp array
            //Emp e1 = new Emp();
            // e1.Name = "rehan";
            //  e1.No = 1;

            //Emp e2 = new Emp();
            //e2.Name = "pratik";
            //e2.No = 2;

            //Emp e3 = new Emp();
            //e3.Name = "yoesh";
            //e3.No = 3;

            //Emp[] array = new Emp[3];

            //array[0] = e1;
            //array[1] = e2;
            //array[2] = e3;

            //for (int i = 0; i < array.Length; i++)
            //{
            //    Emp e = array[i];
            //    Console.WriteLine(e.getDetails());
            //} 
            #endregion

            #region object array
            //Emp e1 = new Emp();
            //e1.Name = "rehan";
            //e1.No = 1;

            //object[] obj = new object[3];

            //obj[0] = 2;
            //obj[1] = "reh";
            //obj[2] = e1;
            //for (int i = 0; i < obj.Length; i++)
            //{
            //    if (obj[i] is int)
            //    {
            //        int p = Convert.ToInt32(obj[i]);
            //        Console.WriteLine(p);
            //    }
            //    else if (obj[i] is string)
            //    {
            //        string s = Convert.ToString(obj[i]);
            //        Console.WriteLine(s);
            //    }
            //    else if (obj[i] is Emp)
            //    {
            //        Emp e = (Emp)obj[i];
            //        Console.WriteLine(e.getDetails());
            //    }
            //} 
            #endregion

            #region Object array
            //Emp e1 = new Emp();
            //e1.Name = "rehan";
            //e1.No = 1;

            //Object[] obj = new Object[3];

            //obj[0] = 2;
            //obj[1] = "reh";
            //obj[2] = e1;
            //for (int i = 0; i < obj.Length; i++)
            //{
            //    if (obj[i] is int)
            //    {
            //        int p = Convert.ToInt32(obj[i]);
            //        Console.WriteLine(p);
            //    }
            //    else if (obj[i] is string)
            //    {
            //        string s = Convert.ToString(obj[i]);
            //        Console.WriteLine(s);
            //    }
            //    else if (obj[i] is Emp)
            //    {
            //        Emp e = (Emp)obj[i];
            //        Console.WriteLine(e.getDetails());
            //    }
            //} 
            #endregion

            #region arraylist
            //Emp e1 = new Emp();
            //e1.Name = "rehan";
            //e1.No = 1;

            //Emp e2 = new Emp();
            //e2.Name = "yogesh";
            //e2.No = 2;

            //ArrayList obj = new ArrayList();

            //obj.Add(2);
            //obj.Add("reh");
            //obj.Add(e1);
            //obj.Add(true);
            //obj.Add(2.2);
            //obj.Add(e2);

            //for (int i = 0; i < obj.Count; i++)
            //{
            //    if (obj[i] is int)
            //    {
            //        int p = Convert.ToInt32(obj[i]);
            //        Console.WriteLine(p);
            //    }
            //    else if (obj[i] is string)
            //    {
            //        string s = Convert.ToString(obj[i]);
            //        Console.WriteLine(s);
            //    }
            //    else if (obj[i] is Emp)
            //    {
            //        Emp e = (Emp)obj[i];
            //        Console.WriteLine(e.getDetails());
            //    }
            //    else
            //    {
            //        Console.WriteLine("unknown");
            //    }
            //} 
            #endregion


            #region HashTable
            //Emp e1 = new Emp();
            //e1.Name = "rehan";
            //e1.No = 1;

            //Emp e2 = new Emp();
            //e2.Name = "yogesh";
            //e2.No = 2;

            //Hashtable obj = new Hashtable();

            //obj.Add("name","rehan");
            //obj.Add("no",23);
            //obj.Add("add","pune");
            //obj.Add("contact",788);

            //foreach (object key in obj.Keys)
            //{
            //    object obj1 = obj[key];
            //    Console.WriteLine(key);


            //    if (obj1 is int)
            //    {
            //        int p = Convert.ToInt32(obj1);
            //        Console.WriteLine(p);
            //    }
            //    else if (obj1 is string)
            //    {
            //        string s = Convert.ToString(obj1);
            //        Console.WriteLine(s);
            //    }
            //    else if (obj1 is Emp)
            //    {
            //        Emp e = (Emp)obj1;
            //        Console.WriteLine(e.getDetails());
            //    }
            //    else
            //    {
            //        Console.WriteLine("unknown");
            //    }
            //} 
            #endregion


            #region search element
            //Emp e1 = new Emp();
            //e1.Name = "rehan";
            //e1.No = 1;

            //Emp e2 = new Emp();
            //e2.Name = "yogesh";
            //e2.No = 2;

            //Hashtable obj = new Hashtable();

            //obj.Add("name", "rehan");
            //obj.Add("no", 23);
            //obj.Add("add", "pune");
            //obj.Add("contact", 788);


            //Console.WriteLine("enter key to search ");
            //string key = Console.ReadLine();

            //object obj1 = obj[key];

            //for (int i = 0; i < obj.Count; i++)
            //{

            //    if (obj1 is int)
            //    {
            //        int p = Convert.ToInt32(obj1);
            //        Console.WriteLine(p);
            //        break;
            //    }
            //    else if (obj1 is string)
            //    {
            //        string s = Convert.ToString(obj1);
            //        Console.WriteLine(s);
            //        break;
            //    }
            //    else if (obj1 is Emp)
            //    {
            //        Emp e = (Emp)obj1;
            //        Console.WriteLine(e.getDetails());
            //        break;
            //    }
            //    else
            //    {
            //        Console.WriteLine("unknown");
            //        break;
            //    }



            //} 
            #endregion







        }
    }
    public class Emp
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return "No: " + this.No.ToString() + " Name: " + this.Name;
        }
    }
}

