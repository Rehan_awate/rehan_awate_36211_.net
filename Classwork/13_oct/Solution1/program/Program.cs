﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace program
{
    class Program
    {
        delegate string mydelegate (string s); 
        static void Main(string[] args)
        {
            #region Partial class
            //Maths m = new Maths();
            //Console.WriteLine(m.Add(1, 2)); 
            #endregion

            #region Nullable
            //int? sal = null;
            //sal = 10;
            //if (sal.HasValue)
            //{
            //    Console.WriteLine("sal is not null");
            //}
            //else Console.WriteLine("sal has null value"); 
            #endregion

            #region anonymous method
            //call method using pointer
            //mydelegate pointer = new mydelegate(src);
            //string s =pointer("rehan");
            //Console.WriteLine(s);

            //call method which is anonymous

            //mydelegate pointer = delegate (int i)
            //{
            //    return i;
            //};

            //Console.WriteLine(pointer(2));

            #endregion


            #region lambda expression
            //mydelegate pointer = (src) =>
            //{

            //    Console.WriteLine(src.GetType().FullName);
            //    return src + " IYI ";
            //};

            //Console.WriteLine(pointer("drillis"));
            #endregion

            #region Var
            //Console.WriteLine("enter anyting");
            //var v =  Console.ReadLine();
            // Emp e = new Emp();

            //Console.WriteLine(v.GetType().FullName);

            // Console.WriteLine("type of v is ");
            //Console.WriteLine( v.GetType()); 
            #endregion

            #region Object initilize list
            //Emp e = new Emp() { NO = 20, EName = "rehan" };
            //Console.WriteLine(" NO is " + e.NO + " name " + e.EName);

            //e.EName = "shubham";

            //Console.WriteLine(" NO is " + e.NO + " name " + e.EName); 
            #endregion

            #region Anonymous type
            //var v1 = new {NO=10,EName="Anrehan" };

            //Console.WriteLine(" NO is " + v1.NO + " name " + v1.EName); 
            #endregion






        }

        public static string src(string s)
        {
            return s;
        }


        
    }
    
    //auto property
   public class Emp
    {
        public string EName { get;  set; }
        public int NO { get; set; }

    }
}
