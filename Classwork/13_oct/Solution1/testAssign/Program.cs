﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testAssign
{
    public delegate string mydelegate();
    class Program
    {
        static void Main(string[] args)
        {
            A aobj = new A();
            B bobj = new B();

            mydelegate pointer = new mydelegate(bobj.M2);
            aobj.M(pointer);
        }
    }

    public class A
    {
       public void M(mydelegate pointer)
        {
            Console.WriteLine(pointer());
        }
    }

    public class B
    {
        public string M2()
        {
            return "M2 from B";
        }

    }
}
