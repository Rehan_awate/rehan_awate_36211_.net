﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _01demo.DAL;
using _01demo.POCO;

namespace _01demo
{
    class Program
    {
        static void Main(string[] args)
        {
            DBFactory dBFactory = new DBFactory();
            IDatabase db = dBFactory.GetDatabase();

            Console.WriteLine("1.select; 2.insert; 3.update; 4.delete");

            int opChoice = Convert.ToInt32(Console.ReadLine());

            switch (opChoice)
            {
                case 1:
                    var data = db.Select();
                    foreach (var item in data)
                    {
                        Console.WriteLine(item.Name + "|" + item.Address);

                    }
                    break;
                case 2:
                    Emp emp = new Emp();

                    Console.WriteLine("enter no");
                    emp.No = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("enter name");
                    emp.Name = Console.ReadLine();

                    Console.WriteLine("enter address");
                    emp.Address = Console.ReadLine();

                    int rowaffected = db.Insert(emp);

                    Console.WriteLine("rows inserted" + rowaffected.ToString());
                    break;

                case 3:
                    Emp emp1 = new Emp();

                    Console.WriteLine("enter no u want to insert");
                    emp1.No = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("enter name u want to insert");
                    emp1.Name = Console.ReadLine();

                    

                    int rowaffected1 = db.Update(emp1);

                    Console.WriteLine("rows inserted" + rowaffected1.ToString());
                    break;

                case 4:

                    Emp emp2 = new Emp();

                    Console.WriteLine("enter no u want to delete");
                    emp2.No = Convert.ToInt32(Console.ReadLine());




                    int rowaffected2 = db.Delete(emp2);

                    Console.WriteLine("rows inserted" + rowaffected2.ToString());
                    break;


            }



        }
    }
}
