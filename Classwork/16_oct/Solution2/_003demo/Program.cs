﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace _003demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Fetch data disconnected archi
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(@" Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=Study;Integrated Security=True;Pooling=False");

            SqlDataAdapter da = new SqlDataAdapter("select * from Employee", con);

            //unique key
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            //build sql query for da.update
            SqlCommandBuilder cmb = new SqlCommandBuilder(da);

            da.Fill(ds, "Employee");
            #endregion

            #region insert using disconnected archi
            //insert a new row 

            //DataRow completeNewRow = ds.Tables["Emp"].NewRow();

            //completeNewRow["Id"] = 70;
            //completeNewRow["Name"] = "aditya";
            //completeNewRow["Address"] = "ranga";


            //ds.Tables[0].Rows.Add(completeNewRow);
            //da.Update(da,"Employee");
            #endregion

            #region update using disconnected
            //Console.WriteLine("nter no");
            //int no = Convert.ToInt32(Console.ReadLine());

            //DataRow refToTheRowBeModified = ds.Tables[0].Rows.Find(no);

            //if (refToTheRowBeModified != null)
            //{
            //    Console.WriteLine("record found");
            //    Console.WriteLine("Name is " + refToTheRowBeModified["Name"].ToString());
            //    Console.WriteLine("Address is " + refToTheRowBeModified["Address"].ToString());

            //    Console.WriteLine("enter name");
            //    refToTheRowBeModified["Name"] = Console.ReadLine();
            //    Console.WriteLine("enter address");
            //    refToTheRowBeModified["Address"] = Console.ReadLine();

            //}

            //else Console.WriteLine("sorry!");

            //da.Update(ds, "Emp");
            #endregion

            #region Delete Using Disconencted Architecture
            Console.WriteLine("Enter a No of a Emp whose record you wish to Delete:");

            int Id = Convert.ToInt32(Console.ReadLine());

            DataRow refToTheRowToBeDeleted = ds.Tables[0].Rows.Find(Id);

            if (refToTheRowToBeDeleted != null)
            {
                Console.WriteLine("Record Found .. Deleting the same..");
                refToTheRowToBeDeleted.Delete();
            }
            else
            {
                Console.WriteLine("Sorry! No record Found");
            }

            da.Update(ds, "Employee");
            #endregion




        }
    }
}
