﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace _03demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Datatable
            //DataTable table = new DataTable("myTable");

            //DataColumn column1 = new DataColumn("No", typeof(int));
            //DataColumn column2 = new DataColumn("Name", typeof(string));
            //DataColumn column3 = new DataColumn("Address", typeof(string));

            //table.Columns.Add(column1);
            //table.Columns.Add(column2);
            //table.Columns.Add(column3);

            //table.PrimaryKey = new DataColumn[] { column1 };

            //DataRow r1 = table.NewRow();

            //r1["NO"] = 40;
            //r1["Name"] = "omkar";
            //r1["Address"] = "sindhudurg";

            //table.Rows.Add(r1); 
            #endregion

            DataSet ds = new DataSet();

            DataTable table = new DataTable("myTable");

            DataColumn column1 = new DataColumn("Id", typeof(int));
            DataColumn column2 = new DataColumn("Name", typeof(string));
            DataColumn column3 = new DataColumn("Address", typeof(string));

            table.Columns.Add(column1);
            table.Columns.Add(column2);
            table.Columns.Add(column3);

            table.PrimaryKey = new DataColumn[] { column1 };

            //-------------------

            SqlConnection con = new SqlConnection(@" Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=Study;Integrated Security=True;Pooling=False");

            SqlCommand cmd = new SqlCommand("select * from Employee", con);

            con.Open();

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataRow r = table.NewRow();
                r["Id"] = Convert.ToInt32(reader["Id"]);
                r["Name"] = reader["Name"].ToString();
                r["Address"] = reader["Address"].ToString();
                table.Rows.Add(r);
            }
            con.Close();

            //add data in sql server

            //----------------
            ds.Tables.Add(table); ///table from sql server

            //-------------

            DataRow completeNewRow = ds.Tables[0].NewRow();
            completeNewRow["Id"] = 50;
            completeNewRow["Name"] = "Akash";
            completeNewRow["Address"] = "hyd";


            ds.Tables[0].Rows.Add(completeNewRow);

            DataTable t2 = new DataTable("Book");

            //DataTable table = new DataTable("myTable");

            DataColumn column4 = new DataColumn("ISBN", typeof(int));
            DataColumn column5 = new DataColumn("Title", typeof(string));
            

            t2.Columns.Add(column4);
            t2.Columns.Add(column5);
            

            
                DataRow r1 = t2.NewRow();
                r1["ISBN"] = 182;
                r1["Title"] = "Let us c++";
               
                table.Rows.Add(r1);
                ds.Tables.Add(t2);//hardcoded data
            





        }
    }
}
