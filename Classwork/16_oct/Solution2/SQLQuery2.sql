﻿USE [Study]
GO

DECLARE	@return_value Int

EXEC	@return_value = [dbo].[spInsert]
		@no = 20,
		@name = N'Shubham',
		@address = N'mumbai'

SELECT	@return_value as 'Return Value'

GO
