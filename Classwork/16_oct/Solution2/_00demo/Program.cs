﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel.Design;
using System.Linq.Expressions;

namespace _00demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region SQL Injection
            //string constr = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            //SqlConnection con = new SqlConnection(constr);

            //Console.WriteLine("Enter Name");

            //string cmdText = "select count(*) from Employee where Name = '" + Console.ReadLine() + "'";

            //SqlCommand cmd = new SqlCommand(cmdText, con);
            //con.Open();

            //object result = cmd.ExecuteScalar();
            //int count = Convert.ToInt32(result);
            //if (count > 0)
            //{
            //    Console.WriteLine("You are a valid user");
            //}
            //else
            //{
            //    Console.WriteLine("You are invalid user!");
            //}

            //SqlDataReader reader = cmd.ExecuteReader();

            //while (reader.Read())
            //{
            //    Console.WriteLine(string.Format("{0} | {1}", reader["Name"], reader["Address"]));
            //}

            // con.Close();
            #endregion


            #region stored procedure
            SqlConnection con = null;

            try
            {
                string constr = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();
                con = new SqlConnection(constr);

                SqlCommand cmd = new SqlCommand("spInsert", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter parameter1 = new SqlParameter("@no", SqlDbType.Int);
                Console.WriteLine("enter no");
                parameter1.Value = Convert.ToInt32(Console.ReadLine());

                SqlParameter parameter2 = new SqlParameter("@name", SqlDbType.VarChar, 50);
                Console.WriteLine("enter name");
                parameter2.Value = Console.ReadLine();

                SqlParameter parameter3 = new SqlParameter("@address", SqlDbType.VarChar, 50);
                Console.WriteLine("enter address");
                parameter3.Value = Console.ReadLine();

                cmd.Parameters.Add(parameter1);
                cmd.Parameters.Add(parameter2);
                cmd.Parameters.Add(parameter3);

                con.Open();

                cmd.ExecuteNonQuery();

                Console.WriteLine("record inserted");
            }
            catch (Exception ex)
            {
                Console.WriteLine("record insertion failed");
                Console.WriteLine(ex.Message);

            }
            finally
            {
                con.Close();
            }
            #endregion













        }
    }
}
