﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region normal derived class object to call generic base class method
            //    //normal derived class object to call generic base class method
            //SpecialMaths sm =  new SpecialMaths();

            //    double p = 01.2; double q = 01.3;
            //    Console.WriteLine("before swap");
            //    Console.WriteLine("p =" + p.ToString() + "q =" + q.ToString() );
            //    sm.Swap(ref p, ref q);

            //         Console.WriteLine("after swap");
            //    Console.WriteLine("p = " + p.ToString() + "q = " + q.ToString()); 
            #endregion

            #region using generic method with multiple parameters
            ////using generic method with multiple parameters
            //SpecialMaths1<int, string, double, bool> obj = new SpecialMaths1<int, string, double, bool>();
            //int p = 12;
            //Console.WriteLine(obj.Print(12, "reha", 12.3, true));    
            #endregion

        }
    }

    public class SpecialMaths : Maths<double>
    {

    }

    public class SpecialMaths1<Q  , P , S , A >
    {
         public P Print(Q q, P p, S s, A a)
        {
            return p;
        }
    }



    public class Maths<T>
    {
        public void Swap(ref T x, ref T y)
        {
            T z;
            z = x;
            x = y;
            y = z;
        }
    }
}
