﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _04demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region normal file Writing
            //FileStream fs = new FileStream(@"C:\rehan_awate_36211_.net\Classwork\8_oct\_00Demo\hello.txt",
            //                                       FileMode.OpenOrCreate,
            //                                       FileAccess.Write);
            //StreamWriter writer = new StreamWriter(fs);
            //writer.WriteLine("hi rehan");

            //fs.Flush();
            //fs.Close(); 
            #endregion

            #region Normal file reading
            //    FileStream fs = new FileStream(@""C:\rehan_awate_36211_.net\Classwork\8_oct\_00Demo\hello.txt",
            //                                    FileMode.Open,
            //                                    FileAccess.Read);

            //    StreamReader reader = new StreamReader(fs);

            //    while (true)
            //    {
            //        string entireData = reader.ReadLine();
            //        if (entireData != null)
            //        {
            //            Console.WriteLine(entireData);
            //        }
            //        else
            //        {
            //            break;
            //        }
            //    }



            //    string entireData = reader.ReadToEnd();
            //    Console.WriteLine(entireData);

            //    reader.Close();
            //    fs.Close();

            //} 
            #endregion


            #region normal file writing with emp object
            //emp emp = new emp();

            //console.writeline("enter no:");
            //emp.no = convert.toint32(console.readline());

            //console.writeline("enter name");
            //emp.name = console.readline();


            //filestream fs = new filestream(@""c:\rehan_awate_36211_.net\classwork\8_oct\_00demo\hello.txt",
            //                                filemode.openorcreate,
            //                                fileaccess.write);

            //streamwriter writer = new streamwriter(fs);

            //writer.writeline(emp);

            //fs.flush();
            //writer.close();
            //fs.close(); 
            #endregion


        }

        public class Emp
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return No.ToString() + " ,  " + Name;
        }

    }
}
