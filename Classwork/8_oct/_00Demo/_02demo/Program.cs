﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02demo
{
    class Program
    {
        static void Main(string[] args)
        {
           A obj = new Maths();
            S obj1 = new Maths();

            int result = obj.Add(10,5);
            Console.WriteLine(result);

            double result1 = obj1.percentage( 5.3 ,10.2);
            Console.WriteLine(result1);

        }
    }

    public interface A
    {
        int Add(int a, int b);
        int Sub(int a, int b);
    }
    public interface B
    {
        int Mul(int a, int c);
        int Div(int a, int c);

    }
    public interface S
    {
        double percentage(double get, double total);
    }

    public class Maths : A, B,S
    {

        int A.Add(int a, int b)
        {
            return a + b;
        }

        int B.Div(int a, int b)
        {
            return a / b;
        }

        int B.Mul(int a, int b)
        {
            return a * b;
        }

        int A.Sub(int a, int b)
        {
            return a / b;
        }


        double S.percentage(double get , double total)
        {
            return (get / total) * 100;
        }
    }
}
