﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace _01Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Emp e1 = new Emp();
            e1.No = 1;
            e1.Name = "rehan";

            Emp e2 = new Emp();
            e2.No = 2;
            e2.Name = "ravi";

            Emp e3 = new Emp();
            e3.No = 3;
            e3.Name = "onkar";

            #region Stack
            //Stack<Emp> arr = new Stack<Emp>();
            //arr.Push(e1);
            //arr.Push(e2);
            //arr.Push(e3);

            //arr.Pop();
            //foreach (Emp e in arr)
            //{
            //    Console.WriteLine(e.getDetails());
            //} 
            #endregion

            #region queue
            //Queue<Emp> arr = new Queue<Emp>();

            //arr.Enqueue(e1);
            //arr.Enqueue(e2);
            //arr.Enqueue(e3);

            //arr.Dequeue();

            //foreach (Emp emp in arr)
            //{
            //    Console.WriteLine(emp.getDetails());
            //} 
            #endregion

            #region Dictionary
            //Dictionary<string, Emp> arr = new Dictionary<string, Emp>();

            //arr.Add("name1",e1);
            //arr.Add("name2", e2);
            //arr.Add("name3", e3);

            //foreach (string  key in arr.Keys)
            //{
            //    Emp e = arr[key];
            //    Console.WriteLine(key + e.getDetails());
            //}

            #endregion

            Maths<double> arr = new Maths<double>();
            double x = 10.2;
            double y= 10.3;


            Console.WriteLine("before swap ");
            Console.WriteLine("x:" + x); Console.WriteLine("y" + y);
            arr.Swap(ref x, ref y);
            Console.WriteLine("after swap ");
            Console.WriteLine("x:" + x); Console.WriteLine("y" + y);







        }
    }

    public class Utility <T>
    {

    }
    public class Maths<T>
    {
        public void Swap(ref T x, ref T y)
        {
            T z;
            z = x;
            x = y;
            y = z;
        }
    }

    public class Emp
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return this.No.ToString() + this.Name;
        }

    }
}
