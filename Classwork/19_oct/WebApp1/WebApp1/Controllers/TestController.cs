﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp1.Models;

namespace WebApp1.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        StudyEntities dbObj = new StudyEntities();
        public ActionResult Show()
        {

            try
            {
                var allEmps = dbObj.Employees.ToList();
                return View(allEmps);
            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }


            //Emp e1 = new Emp() { No=1, Name="rehan",Address="goa" };

            //List<Emp> allEmps = new List<Emp>() { 
            //    new Emp{ No=1,Name="rehan",Address="Pune"},
            //    new Emp{ No=2,Name="onkar",Address="kank"},
            //    new Emp{ No=3,Name="omkar",Address="Pune1"}


            //};
          
        }

        public ActionResult Delete(int id)
        {

            try
            {
                Employee ToBeDelete = (from emp in dbObj.Employees.ToList()
                                       where emp.Id == id
                                       select emp).First();

                dbObj.Employees.Remove(ToBeDelete);
                dbObj.SaveChanges();
                return Redirect("/Test/Show");

            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }
        }

        public ActionResult Edit(int id)
        
        {

            try
            {
               Employee empToBeEdited = (from emp in dbObj.Employees.ToList()
                                     where emp.Id == id
                                     select emp).First();

                return View(empToBeEdited);
            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }
        }

        public ActionResult AfterEdit(FormCollection entireForm)
        {
          //  try
            //{
                int NoOfTheEmpToEdit = Convert.ToInt32(entireForm["Id"]);

            //below query is giving you reference and not a copy!
            Employee empToBeEdited1 = (from emp in dbObj.Employees.ToList()
                                      where emp.Id == NoOfTheEmpToEdit
                                     select emp).First();

                empToBeEdited1.Name = entireForm["Name"].ToString();
                empToBeEdited1.Address = entireForm["Address"].ToString();

                dbObj.SaveChanges();//This will update the database

                return Redirect("/Test/Show");
           // }
            //catch (Exception ex)
            //{
             //   return View("Error", ex);
            //}
        }

        public ActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }
        }

        public ActionResult AfterCreate(FormCollection entireForm)
        {
            try
            {
                Employee empToBeAdded = new Employee()
                {
                    Id = Convert.ToInt32(entireForm["No"]),
                    Name = entireForm["Name"].ToString(),
                    Address = entireForm["Address"].ToString()
                };

                dbObj.Employees.Add(empToBeAdded);
                dbObj.SaveChanges();

                return Redirect("/Test/Show");
            }
            catch (Exception ex)
            {

              
                return View("Error", ex);
            }
        }


    }


}