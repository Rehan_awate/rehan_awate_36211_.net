﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00demo
{
    class Program
    {
        static void Main(string[] args)
        {
            StudyEntities dbobject = new StudyEntities();

            #region select using entity framework
            //var allemps = dbobject.Employees.ToList();

            //foreach (var emp in allemps)
            //{
            //    Console.WriteLine(emp.Name + "|" + emp.Address);
            //}

            #endregion

            #region Insert using framework
            //insert into table
            //dbobject.Employees.Add(new Employee() {Id = 80 ,Name="turgut",Address = "turkey" });


            //dbobject.SaveChanges();

            #endregion

            #region Update EF
            //var empToModify = (from emp in dbobject.Employees.ToList()
            //                   where emp.Id == 30
            //                   select emp).First();

            //empToModify.Name = "nurgul"; 
            #endregion

            #region Delete by Ef
            //var empToDelete = (from emp in dbobject.Employees.ToList()
            //                   where emp.Id == 30
            //                   select emp).First();

            //dbobject.Employees.Remove(empToDelete);


            //dbobject.SaveChanges();
            //Console.WriteLine("deletion done");

            #endregion

           // dbobject.spUpdate(80,"nurgul");
            dbobject.DeleteOfOrder(1,"Pizza",23.3);

        }
    }
}
