﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter choice 1.Pdf 2.Word 3.Excel 4.Txt");
            int ch = Convert.ToInt32(Console.ReadLine());
            
            ObjFact obj1 = new ObjFact();
            Report r1 = obj1.GetMeObj(ch);
            r1.Generate();
            Console.ReadLine();

        }
    }

    public class ObjFact
    {
        public Report GetMeObj(int ch)
        {
            if (ch == 1) return new Pdf();

            else if (ch == 2) return new Word();
            else if (ch == 3) return new Excel();
            else return new Txt();
        }
      

    }
    public abstract class Report
    {
        public abstract void Parse();
    public abstract void Validate();
    public abstract void Save();

    public virtual void Generate()
    {
        Parse();
        Validate();
        Save();
    }
     }
    public class Pdf : Report
    {
        public override void Parse()
        {
            Console.WriteLine("Parse in Pdf");
        }
        public override void Validate()
        {
            Console.WriteLine("Validate in Pdf");
        }
        public override void Save()
        {
            Console.WriteLine("Save in Pdf");
        }

    }
    public class Word : Report
    {
        public override void Parse()
        {
            Console.WriteLine("Parse in Word");
        }
        public override void Validate()
        {
            Console.WriteLine("Validate in Word");
        }
        public override void Save()
        {
            Console.WriteLine("Save in Word");
        }

    }
    public class Excel :Report
    {
        public override void Parse()
        {
            Console.WriteLine("Parse in Excel");
        }
        public override void Validate()
        {
            Console.WriteLine("Validate in Excel");
        }
        public override void Save()
        {
            Console.WriteLine("Save in Excel");
        }

    }
    public class Txt: Report
    {
        public override void Parse()
        {
            Console.WriteLine("Parse in Txt");
        }
        public override void Validate()
        {
            Console.WriteLine("Validate in Txt");
        }

        public  void ReSave()
        {
            Console.WriteLine("reSave in Txt");
        }
        public override void Save()
        {
            Console.WriteLine("Save in Txt");
        }

        public override void Generate()

        {
            Parse();
            Validate();
            ReSave();
            Save();

        }

    }
}
