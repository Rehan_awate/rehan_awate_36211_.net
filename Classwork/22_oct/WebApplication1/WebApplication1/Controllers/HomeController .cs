﻿using System;
using System.Collections.Generic;
using System.Data.Metadata.Edm;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using System.Configuration;
using System.Runtime.CompilerServices;
using WebApplication1.Filters;

namespace WebApplication1.Controllers
{
   
    public class HomeController : BaseController
    {
       
        
       
        public ActionResult Index()
        {
            ViewBag.MyTitle = "Index";
            ViewBag.MyMessage = "Welcome to employee management";
            ViewBag.MyMessage = "This is HRMS";
            ViewBag.UserName = User.Identity.Name ;

            var allEmps = dbObj.Employees.ToList();
            return View(allEmps);
        }
        
        //allow without authorize
        [AllowAnonymous]
        public ActionResult About()
        {
            ViewBag.MyTitle = "AboutUs";
            return View();
        }

        public ActionResult Delete(int id)
        {

              Employee ToBeDelete = (from emp in dbObj.Employees.ToList()
                                       where emp.Id == id
                                       select emp).First();

                dbObj.Employees.Remove(ToBeDelete);
                dbObj.SaveChanges();
                return Redirect("/Home/Index");

        }

        public ActionResult Edit(int id)

        {

            
                ViewBag.MyMessage = "Edit register";
                Employee empToBeEdited = (from emp in dbObj.Employees.ToList()
                                          where emp.Id == id
                                          select emp).First();

                return View(empToBeEdited);
           
        }

        [HttpPost]
        public ActionResult Edit(Employee empUpdated)
        {
            


                Employee empToBeEdited1 = (from emp in dbObj.Employees.ToList()
                                           where emp.Id == empUpdated.Id
                                           select emp).First();

                empToBeEdited1.Name = empUpdated.Name;
                empToBeEdited1.Address = empUpdated.Address;

                dbObj.SaveChanges();//This will update the database

                return Redirect("/Home/Index");
           
        }


        #region BY Get method
        //public ActionResult AfterEdit(FormCollection entireForm)
        //{
        //    try
        //    {
        //        int NoOfTheEmpToEdit = Convert.ToInt32(entireForm["Id"]);

        //    //below query is giving you reference and not a copy!

        //    Employee empToBeEdited1 = (from emp in  dbObj.Employees.ToList()
        //                               where emp.Id == NoOfTheEmpToEdit
        //                               select emp).First();

        //    empToBeEdited1.Name = entireForm["Name"].ToString();
        //    empToBeEdited1.Address = entireForm["Address"].ToString();

        //    dbObj.SaveChanges();//This will update the database

        //    return Redirect("/Test/Show");
        //   }
        //    catch (Exception ex)
        //    {
        //       return View("Error", ex);
        //    }
        // } 
        #endregion


        public ActionResult AfterEdit(Employee empUpdated)
        {
            


                Employee empToBeEdited1 = (from emp in dbObj.Employees.ToList()
                                           where emp.Id == empUpdated.Id
                                           select emp).First();

                empToBeEdited1.Name = empUpdated.Name;
                empToBeEdited1.Address = empUpdated.Address;

                dbObj.SaveChanges();//This will update the database

                return Redirect("/Home/Index");
           
        }
        public ActionResult Create()
        {
            
                ViewBag.MyMessage = "crete profile";
                return View();
          
        }

        [HttpPost]
        public ActionResult Create(Employee empUpdated)
        {
           

                dbObj.Employees.Add(empUpdated);
                dbObj.SaveChanges();

                return Redirect("/Home/Index");
            
        }

        #region By get method
        //public ActionResult AfterCreate(FormCollection entireForm)
        //{
        //    try
        //    {
        //        Employee empToBeAdded = new Employee()
        //        {
        //            Id = Convert.ToInt32(entireForm["No"]),
        //            Name = entireForm["Name"].ToString(),
        //            Address = entireForm["Address"].ToString()
        //        };

        //        dbObj.Employees.Add(empToBeAdded);
        //        dbObj.SaveChanges();

        //        return Redirect("/Test/Show");
        //    }
        //    catch (Exception ex)
        //    {


        //        return View("Error", ex);
        //    }
        //}
        #endregion


        public ActionResult AfterCreate(Employee empUpdated)
        {
            

                dbObj.Employees.Add(empUpdated);
                dbObj.SaveChanges();

                return Redirect("/Home/Index");
            
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.MyTitle = "ContactUs";
            ViewBag.action = "/Home/Contact";
            ViewBag.method = "POST";
            ViewBag.message = "error incase of problem";
            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactModel ContactDetails)
        {
            
                string emailUser = ConfigurationManager.AppSettings["email"];
                string passwordUser = ConfigurationManager.AppSettings["password"];
               
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(emailUser);
                mail.To.Add(ContactDetails.Email);
              
                mail.Subject = "new query received";
                mail.Body ="<h2>" + ContactDetails.ToString() + "</h2>";
                mail.IsBodyHtml = true;

                //SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                //smtp.Credentials = new NetworkCredential(emailUser, passwordUser);
                //smtp.EnableSsl = true;
                //smtp.Send(mail);
                //ViewBag.message = "ur query submitted succesfully";

                //smtp.UseDefaultCredentials = false;

                //smtp.EnableSsl = true;
                //smtp.UseDefaultCredentials = false;

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");
                smtp.Credentials = new System.Net.NetworkCredential(emailUser, passwordUser);
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com";
                smtp.Send(mail);


                return View();
            
            
        }
    }
}