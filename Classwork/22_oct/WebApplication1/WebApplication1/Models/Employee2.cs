﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApplication1.Helpers;
namespace WebApplication1.Models
{ 
  [MetadataType (typeof(Extra_Info_About_Employee))]
    public partial class Employee 
    {  
       
    }

    public partial class Extra_Info_About_Employee
    {
        [Required(ErrorMessage = "NO")]
        [Range(1, 100, ErrorMessage = "No must be in 1 to 100")]
        public int Id { get; set; }

        [SBValidate(ErrorMessage = "1234 is not valid")]
        [Required(ErrorMessage = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Address")]
        public string Address { get; set; }






    }
}