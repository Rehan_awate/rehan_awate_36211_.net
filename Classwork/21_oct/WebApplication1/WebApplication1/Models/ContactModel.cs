﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ContactModel
    {
        public string Name { get; set; }
        public int Phone { get;  set; }
        public string Email { get;  set; }
        public string Query { get;  set; }
      
        public override string ToString()
        {
            return string.Format("hello {0} ,we received ur query as phone:{1},Email:{2},query:{3}",this.Name,this.Phone,this.Email,this.Query);
        }
    }
}