﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using System.Web.Security;
using WebApplication1.Filters;

namespace WebApplication1.Controllers
{
    public class LoginController : Controller
    {
        sunbeamdbEntities dbObj = new sunbeamdbEntities();
        // GET: Login
        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(HRLoginInfo loginObject,string ReturnUrl)
        {
            
            var MathchCount = (from hr in dbObj.HRLoginInfoes.ToList()
                          where  hr.UserName.ToLower() == loginObject.UserName.ToLower() 
                          && hr.Password == loginObject.Password 
                          select hr).ToList().Count();

            if (MathchCount == 1 )
            {   
                 // if use name is correct
                //create session... send cookies to  client
                FormsAuthentication.SetAuthCookie(loginObject.UserName, false);

                if (ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("/Home/Index");
                }
            }
            else
            {
                ViewBag.ErrorMessage = "Username or password is wrong";
            }
            return View();
        }

        public ActionResult Signout()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/Index");
        }
    }
}