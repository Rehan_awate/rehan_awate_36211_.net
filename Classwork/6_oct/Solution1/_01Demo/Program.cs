﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            CousultantEmployee c1 = new CousultantEmployee();
            c1.name = "rehan";
            c1.age = 23;
            c1.dname = "ece";
            c1.hr = 2;

            Console.WriteLine(c1.PrintDetails());
        }
    }
    public class Person
    {
        private string _Name;
        private int _Age;

        public string name
        {
            get { return this._Name; }
            set { this._Name = value; }
        }

        public int age
        {
            get { return this._Age; }
            set { this._Age = value; }
        }

        public Person()
        {
            this._Name = "";
            
            this._Age = 0;
        } 
        public Person (string name,int age)
        {
            this._Name = name;
            this._Age = age;
        }

        public virtual string PrintDetails()
        {
            return "name is " + this._Name + "Age is " + this._Age.ToString();
        }
    }
     class Employee : Person
        {
            private string _Dname;
            public string dname
            {
                get { return _Dname; }
                set { _Dname = value; }
            }

            public override string PrintDetails()
            {
                return base.PrintDetails() +  " dname " + this._Dname;
             }
        }

     class CousultantEmployee : Employee
        {
            private int _WorkingHours;

            public int hr
            {
                get { return _WorkingHours; }
                set { _WorkingHours = value; }
            }

            public override string PrintDetails()
            {
                return base.PrintDetails() + " working hr is " + this._WorkingHours;
            }
        }


    
}
