﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{
    class Program 
    { 

        #region mainfunction
    static void Main(string[] args)
    {
        Person p1 = new Person();
       // p1.Name = "rehan";
        p1.Age = 23;

        Console.WriteLine(p1.PrintDetails());
    }
    #endregion

}
    class Person
    {
        private string _Name;




        /// <summary>
        /// setter function 
        /// p1Object.set_Name("john")
        /// </summary>
        /// <param name="Name"> this is name of the person </param>

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public void Set_Name()
        {
            this._Name = _Name;
        }

        private int _Age;

        public int Age
        {
            get { return _Age; }
            set { _Age = value; }
        }
        public string PrintDetails()
        {
            return "name is " + this._Name + " age is " + this._Age;
        }



    }
}
