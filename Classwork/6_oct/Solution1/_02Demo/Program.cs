﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace _02Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.CurrentLogger.Log("main called");
            Emp e1 = new Emp();
            e1.GetDetails();


            Maths m1 = new Maths();

            Console.WriteLine("enter x");
            int x = Convert.ToInt32(     Console.ReadLine());

            Console.WriteLine("enter y");
            int y = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(m1.Add(x,y));


        }
    }

    public class Emp
    {
        public void GetDetails()
        {
            Logger.CurrentLogger.Log("emp details gettig called");
        
        }
    }

    public class Maths
    {
        public int Add(int x, int y)
        {
            Logger.CurrentLogger.Log( x.ToString() + "+" +  y.ToString());
            return x + y;
        }
    }

    public class Logger
    {
        public static Logger _logger = new Logger();

        private Logger()
        {
            Console.WriteLine(" logger created "); 
        }

        public static   Logger CurrentLogger
        {
            get { return _logger; }
           
        }
        public void Log(string msg)
        {
            Console.WriteLine(" Logged " +
                msg);
        }

    }
}
