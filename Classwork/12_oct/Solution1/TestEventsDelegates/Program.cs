﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB;


namespace TestEventsDelegates
{
   //client 1
    class Program
    {
        static void Main(string[] args)
        {
            SQLServer dbObj = new SQLServer();

            mydelegate pointer1 = new mydelegate(OnInsertCallMe);
            mydelegate pointer2 = new mydelegate(OnUpdateCallMe);
            mydelegate pointer3 = new mydelegate(OnDeleteCallMe);

            dbObj.Inserted += pointer1;
            dbObj.Updated += pointer2;
            dbObj.Deleted += pointer3;

            dbObj.Insert("db insert");
          //  dbObj.Update("db update");
            //dbObj.Delete("db delete");


        }

        public static void OnInsertCallMe()
        {
            Console.WriteLine("Logging Insert into Console");
        }

        public static void OnUpdateCallMe()
        {
            Console.WriteLine("Logging Update into Console");
        }

        public static void OnDeleteCallMe()
        {
            Console.WriteLine("Logging Delete into Console");
        }
    }


}

namespace DB
{

    public delegate void mydelegate();

    //developer1
    
    public class SQLServer
    {

        public event mydelegate Inserted;
        public event mydelegate Updated;
        public event mydelegate Deleted;



        public void Insert(string data)
        {
            Console.WriteLine(data + " inserted into SQL Server");
            //Raise Event .. just like raise trigger in database
            Inserted();
        }

        public void Update(string data)
        {
            Console.WriteLine(data + " updated into SQL Server");
            //Raise Event .. just like raise trigger in database
            Updated();
        }

        public void Delete(string data)
        {
            Console.WriteLine(data + " Delete from SQL Server");
            Deleted();
        }
    }
}
