﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace mainprogram2
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("enter the assembly path : exe/dll built with .NET");
            string pathOfAssembly = Console.ReadLine();

            Assembly assembly = Assembly.LoadFrom(pathOfAssembly);
            Type[] allTypes = assembly.GetTypes();



            object dynamicObject = null;

            // assembly.CreateInstance();

            foreach (Type type in allTypes)
            {
                Console.WriteLine("Type: " + type.Name);

               List <Attribute> allAtrributes =  type.GetCustomAttributes().ToList();
                bool isItSerializable = false;

                foreach (Attribute attribute in allAtrributes)
                {
                    if (attribute is SerializableAttribute)
                    {
                        isItSerializable = true;
                        break;
                    } 
                }

                if (isItSerializable)
                {
                    Console.WriteLine(type.Name + "is serializable");
                }
                else
                {
                    Console.WriteLine(type.Name + "is not serializable");
                }
                
                dynamicObject = assembly.CreateInstance(type.FullName);
                MethodInfo[] allMethods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MethodInfo method in allMethods)
                {
                    Console.WriteLine(" calling " + method.Name + " method .... ");

                    ParameterInfo[] allParams = method.GetParameters();

                    object[] arguments = new object[allParams.Length];


                    for (int i = 0; i < allParams.Length; i++)
                    {

                         Console.WriteLine(" Enter " + allParams[i].ParameterType.ToString() +   " value  for " + allParams[i].Name);
                        arguments[i] = Convert.ChangeType(Console.ReadLine(),
                            allParams[i].ParameterType);
                       
                    }
                    
                   // object[] allParas = new object[] { 10, 7 };
                    object result = type.InvokeMember(method.Name, BindingFlags.Public
                                    | BindingFlags.Instance
                                    | BindingFlags.InvokeMethod, null
                                     , dynamicObject, arguments);
                    Console.WriteLine(" result of " + method.Name + " executed is " + result.ToString());
                }
            }



        }
    }
}
