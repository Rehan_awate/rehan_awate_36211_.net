﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDelegates
{
    delegate int mydelegate(int p, int q);
    delegate void yourdelegate(string s);

    class Program
    {
        static void Main(string[] args)
        {

            mydelegate pointer = new mydelegate(Sub);
            int result = pointer(2, 4);

            Console.WriteLine(result);

            yourdelegate pointer1 = new yourdelegate(SayHi);

            pointer1("rehan");
            //Console.WriteLine();


        }

        public static void SomeFunction()
        {
           
        }
        public static int Add(int x, int y)
        {
            return x + y;
        }

        public static int Sub(int x, int y)
        {
            return x - y;
        }

        public static void SayHi(string name)
        {
            Console.WriteLine("Hi " + name);
        }
    }
}
