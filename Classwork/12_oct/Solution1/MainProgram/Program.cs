﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace MainProgram
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("enter the assembly path : exe/dll built with .NET");
            string pathOfAssembly = Console.ReadLine();

            Assembly assembly = Assembly.LoadFrom(pathOfAssembly);
            Type[]  allTypes = assembly.GetTypes();

           object dynamicObject = null;

           // assembly.CreateInstance();

            foreach (Type type in allTypes)
            {
                Console.WriteLine("Type: " + type.Name);

                dynamicObject = assembly.CreateInstance(type.FullName);  
                MethodInfo[] allMethods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MethodInfo method in allMethods)
                {
                    Console.WriteLine(method.ReturnType + "" + method.Name + "(");

                   ParameterInfo[] allParams = method.GetParameters();

                    foreach (ParameterInfo para in allParams)
                    {
                        Console.WriteLine(para.ParameterType.ToString() + "" + para.Name + "");
                    }
                    Console.Write(")");
                    Console.WriteLine();
                    object[] allParas = new object[] { 10, 7 };
                      object result =       type.InvokeMember(method.Name, BindingFlags.Public 
                                      | BindingFlags.Instance 
                                      | BindingFlags.InvokeMethod,null
                                       ,dynamicObject, allParas);
                    Console.WriteLine(" result of "  + method.Name + " executed is " + result.ToString());
                }
            }
          


        }
    }
}
