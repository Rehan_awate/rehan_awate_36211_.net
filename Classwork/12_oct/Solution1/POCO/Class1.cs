﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using myownattributes;

namespace POCO
{
    [Table (TableName ="Employee")]
    public class Emp
    {
        private int _NO;
        private string _Name;


        [Column (ColumnName ="EName", ColumnType ="varchar(50)" )]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        [Column(ColumnName = "ENo", ColumnType = "int")]
        public int NO
        {
            get { return _NO; }
            set { _NO = value; }
        }

    }

    

}
