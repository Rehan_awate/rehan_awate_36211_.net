﻿using System;
using System.Collections.Generic;
using System.Data.Metadata.Edm;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using System.Configuration;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            ViewBag.MyTitle = "Index";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.MyTitle = "AboutUs";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.MyTitle = "ContactUs";
            ViewBag.action = "/Home/Contact";
            ViewBag.method = "POST";
            ViewBag.message = "error incase of problem";
            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactModel ContactDetails)
        {
            try
            {
                string emailUser = ConfigurationManager.AppSettings["email"];
                string passwordUser = ConfigurationManager.AppSettings["password"];
               
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(emailUser);
                mail.To.Add(ContactDetails.Email);
              
                mail.Subject = "new query received";
                mail.Body ="<h2>" + ContactDetails.ToString() + "</h2>";
                mail.IsBodyHtml = true;

                //SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                //smtp.Credentials = new NetworkCredential(emailUser, passwordUser);
                //smtp.EnableSsl = true;
                //smtp.Send(mail);
                //ViewBag.message = "ur query submitted succesfully";

                //smtp.UseDefaultCredentials = false;

                //smtp.EnableSsl = true;
                //smtp.UseDefaultCredentials = false;

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");
                smtp.Credentials = new System.Net.NetworkCredential(emailUser, passwordUser);
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com";
                smtp.Send(mail);


                return View();
              }
            catch (Exception ex)
            {

                ViewBag.message = "hi" + ex.Message;
                return View();
            }
            
        }
    }
}