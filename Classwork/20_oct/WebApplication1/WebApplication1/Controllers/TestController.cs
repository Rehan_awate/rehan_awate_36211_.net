﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        sunbeamdbEntities dbObj = new sunbeamdbEntities();
        public ActionResult Show()
        {

            try
            {
                ViewBag.MyMessage = "welcome to my register";
               var allEmps = dbObj.Employees.ToList();
                return View(allEmps);
            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }


            //Emp e1 = new Emp() { No=1, Name="rehan",Address="goa" };

            //List<Emp> allEmps = new List<Emp>() { 
            //    new Emp{ No=1,Name="rehan",Address="Pune"},
            //    new Emp{ No=2,Name="onkar",Address="kank"},
            //    new Emp{ No=3,Name="omkar",Address="Pune1"}


            //};

        }

        public ActionResult Delete(int id)
        {

            try
            {
              Employee ToBeDelete = (from emp in dbObj.Employees.ToList()
                where emp.Id == id
                                       select emp).First();

                dbObj.Employees.Remove(ToBeDelete);
                dbObj.SaveChanges();
                return Redirect("/Test/Show");

            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }
        }

        public ActionResult Edit(int id)

        {

            try
            {
                ViewBag.MyMessage = "Edit register";
                Employee empToBeEdited = (from emp in dbObj.Employees.ToList()
                                          where emp.Id == id
                                          select emp).First();

                return View(empToBeEdited);
            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }
        }

        [HttpPost]
        public ActionResult Edit(Employee empUpdated)
        {
            try
            {


                Employee empToBeEdited1 = (from emp in dbObj.Employees.ToList()
                                           where emp.Id == empUpdated.Id
                                           select emp).First();

                empToBeEdited1.Name = empUpdated.Name;
                empToBeEdited1.Address = empUpdated.Address;

                dbObj.SaveChanges();//This will update the database

                return Redirect("/Test/Show");
            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }
        }


        #region BY Get method
        //public ActionResult AfterEdit(FormCollection entireForm)
        //{
        //    try
        //    {
        //        int NoOfTheEmpToEdit = Convert.ToInt32(entireForm["Id"]);

        //    //below query is giving you reference and not a copy!

        //    Employee empToBeEdited1 = (from emp in  dbObj.Employees.ToList()
        //                               where emp.Id == NoOfTheEmpToEdit
        //                               select emp).First();

        //    empToBeEdited1.Name = entireForm["Name"].ToString();
        //    empToBeEdited1.Address = entireForm["Address"].ToString();

        //    dbObj.SaveChanges();//This will update the database

        //    return Redirect("/Test/Show");
        //   }
        //    catch (Exception ex)
        //    {
        //       return View("Error", ex);
        //    }
        // } 
        #endregion


        public ActionResult AfterEdit(Employee empUpdated)
        {
            try
            {
              

                Employee empToBeEdited1 = (from emp in dbObj.Employees.ToList()
                                           where emp.Id == empUpdated.Id
                                           select emp).First();

                empToBeEdited1.Name = empUpdated.Name;
                empToBeEdited1.Address = empUpdated.Address;

                dbObj.SaveChanges();//This will update the database

                return Redirect("/Test/Show");
            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }
        }
        public ActionResult Create()
        {
            try
            {
                ViewBag.MyMessage = "crete profile";
                return View();
            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }
        }

        [HttpPost]
        public ActionResult Create(Employee empUpdated)
        {
            try
            {

                dbObj.Employees.Add(empUpdated);
                dbObj.SaveChanges();

                return Redirect("/Test/Show");
            }
            catch (Exception ex)
            {


                return View("Error", ex);
            }
        }

        #region By get method
        //public ActionResult AfterCreate(FormCollection entireForm)
        //{
        //    try
        //    {
        //        Employee empToBeAdded = new Employee()
        //        {
        //            Id = Convert.ToInt32(entireForm["No"]),
        //            Name = entireForm["Name"].ToString(),
        //            Address = entireForm["Address"].ToString()
        //        };

        //        dbObj.Employees.Add(empToBeAdded);
        //        dbObj.SaveChanges();

        //        return Redirect("/Test/Show");
        //    }
        //    catch (Exception ex)
        //    {


        //        return View("Error", ex);
        //    }
        //}
        #endregion


        public ActionResult AfterCreate(Employee empUpdated)
        {
            try
            {
             
                dbObj.Employees.Add(empUpdated);
                dbObj.SaveChanges();

                return Redirect("/Test/Show");
            }
            catch (Exception ex)
            {


                return View("Error", ex);
            }
        }


    }
}