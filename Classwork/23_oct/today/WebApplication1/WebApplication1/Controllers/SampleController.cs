﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;


namespace WebApplication1.Controllers
{
    public class SampleController : BaseController
    {
       // sunbeamdbEntities dbObj = new sunbeamdbEntities();
        // GET: Sample
        public ActionResult Index()
        {
            ViewBag.MyTitle = "Index";
            ViewBag.MyMessage = "Welcome to employee management";
            ViewBag.MyMessage = "This is HRMS";
            ViewBag.UserName = User.Identity.Name;

            var allEmps = dbObj.Employees.ToList();
            return View(allEmps);
        }

        //allow without authorize
        [AllowAnonymous]
       
        public ActionResult Delete(int id)
        {

            Employee ToBeDelete = (from emp in dbObj.Employees.ToList()
                                   where emp.Id == id
                                   select emp).First();

            dbObj.Employees.Remove(ToBeDelete);
            dbObj.SaveChanges();
            return Redirect("/Sample/Index");

        }

        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id)

        {


            ViewBag.MyMessage = "Edit register";
            Employee empToBeEdited = (from emp in dbObj.Employees.ToList()
                                      where emp.Id == id
                                      select emp).First();

            return View(empToBeEdited);

        }

        [HttpPost]
        public ActionResult Edit(Employee empUpdated)
        {



            Employee empToBeEdited1 = (from emp in dbObj.Employees.ToList()
                                       where emp.Id == empUpdated.Id
                                       select emp).First();

            empToBeEdited1.Name = empUpdated.Name;
            empToBeEdited1.Address = empUpdated.Address;

            dbObj.SaveChanges();//This will update the database

            return Redirect("/Sample/Index");

        }

        public ActionResult Create()
        {

            ViewBag.MyMessage = "crete profile";
            return View();

        }

        [HttpPost]
        public ActionResult Create(Employee empUpdated)
        {
            if (ModelState.IsValid)
            {
                dbObj.Employees.Add(empUpdated);
                dbObj.SaveChanges();

                return Redirect("/Sample/Index");

            }
            else
            {
                return View(empUpdated);
            }


        }

        


       

    }
}