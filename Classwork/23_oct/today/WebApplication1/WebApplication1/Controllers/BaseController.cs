﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.Filters;
using System.Data.SqlClient;

namespace WebApplication1.Controllers
{
    //[Authorize]
   // [CustomFilter]
   
    //[HandleError(ExceptionType = typeof(SqlException), View ="Error1")]
    //[HandleError(ExceptionType = typeof(ArgumentException), View = "Error2")]
    //[HandleError(ExceptionType = typeof(DivideByZeroException), View = "Error3")]
    //[HandleError(ExceptionType = typeof(Exception), View = "Error4")]
    
    public class BaseController : Controller
    {
        protected sunbeamdbEntities dbObj { get; set; }

        public BaseController()
        {
            this.dbObj = new sunbeamdbEntities();
        }

    }
}