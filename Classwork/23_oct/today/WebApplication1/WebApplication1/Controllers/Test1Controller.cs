﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class Test1Controller : BaseController
    {
        // GET: Test1
        public ActionResult Index(int ? id )
        {
            ActionResult result = null;
            switch (id)
            {
                case 1:
                    Employee emp = new Employee() { Id = 100, Name = "Rajat", Address = "Pune" };
                    result = View(emp);
                    break;
                case 2:
                    result = new JsonResult()
                    {
                        Data = dbObj.Employees.ToList(),
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                    break;
                case 3:
                    result = new JavaScriptResult() { Script = "alert('Hello from JS');" };
                    break;
                case 4:
                    return View("Display");
                default:
                    result = new ContentResult() { ContentType = "text/plain", Content = "Hello from Server" };
                    break;
            }
            return result;
        }
    }
}