﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication1.Models;

namespace DemoMVC.Controllers
{
    public class EmployeeController : ApiController
    {
        private SunbeamdbEntities db = new SunbeamdbEntities();

        // GET: api/Emps
        public IQueryable<Employee> GetEmps()
        {
            return db.Emps;
        }

        // GET: api/Emps/5
        [ResponseType(typeof(Employee))]
        public IHttpActionResult GetEmp(int id)
        {
            Emp Employee = db.Emps.Find(id);
            if (Employee == null)
            {
                return NotFound();
            }

            return Ok(Employee);
        }

        // PUT: api/Emps/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEmp(int id, Employee emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Employee.No)
            {
                return BadRequest();
            }

            db.Entry(Employee).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmpExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Emps
        [ResponseType(typeof(Employee))]
        public IHttpActionResult PostEmp(Employee emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Emps.Add(emp);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (EmpExists(emp.No))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        // DELETE: api/Emps/5
        [ResponseType(typeof(Employee))]
        public IHttpActionResult DeleteEmp(int id)
        {
            Employee emp = db.Emps.Find(id);
            if (emp == null)
            {
                return NotFound();
            }

            db.Emps.Remove(emp);
            db.SaveChanges();

            return Ok(emp);
        }


        private bool EmpExists(int id)
        {
            return db.Emps.Count(e => e.No == id) > 0;
        }
    }
}