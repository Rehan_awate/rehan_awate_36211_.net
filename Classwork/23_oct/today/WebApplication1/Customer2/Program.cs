﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer2
{
    class Program
    {
        static void Main(string[] args)
        {
            DataWebServiceSoapClient proxyObj = new DataWebServiceSoapClient();
            int result = proxyObj.Mul(10, 20);
            Console.WriteLine(result);
        }
    }
}
