﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo02
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Emp> emps = new List<Emp>()
            {
            new Emp { No = 7, Name = "rehan", Address = "pune" },
                new Emp { No = 12, Name = "p1", Address = "panji" },
                new Emp { No = 13, Name = "p2", Address = "mumbai" },
                new Emp { No = 14, Name = "p3", Address = "manglore" },
                new Emp { No = 15, Name = "p4", Address = "banglore" },
                new Emp { No = 16, Name = "p5", Address = "chennai" },
                new Emp { No = 17, Name = "p6", Address = "pune" },
                new Emp { No = 18, Name = "p7", Address = "satara" },
                new Emp { No = 19, Name = "p8", Address = "kolhapur" },
                new Emp { No = 20, Name = "p9", Address = "chennai" }
            };

            Console.WriteLine("enter city to search");
            string cityFilter = Console.ReadLine();

           // var result = new List<Emp>();

            //foreach(Emp emp in emps)
            //{
            //    if( emp.Address.Contains(cityFilter))
            //    {
            //        result.Add(emp);
            //    }
            //}
            //----------------------------------------
            
            
            //by query

            var result =( from emp in emps
                        where emp.Address.Contains(cityFilter)
                      select  new
                      {
                          EName = "mr/mrs" + emp.Name,
                          Ecity = emp.Address
                      } 
                     ).ToList();

              emps.Add(   new Emp { No = 20, Name = "p10", Address = "jaipur" });

            Console.WriteLine("result is :");

            foreach (var e in result)
            {
                Console.WriteLine( " name " + e.EName + " address is " + e.Ecity );
            }
            

        }
    }

    public class ResultHolder
    {
        public int EName { get;  set; }
        public string ECity { get;  set; }

    }
    public class Emp
    {
        public int No { get;  set; }
        public string Name { get;  set; }
        public string Address { get;  set; }
    }
}
