﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Linq.Expressions;

namespace Linq
{
    //public delegate bool mydelagate(int i);
    public delegate Q mydelagate <Q,T> (T i);
    class Program
    {
        static void Main(string[] args)
        {
            // mydelagate pointer = new mydelagate(Chake);
            //mydelagate<bool,int> pointer = new mydelagate<bool,int>(Chake);
            //mydelagate<bool, int> pointer = delegate (int i)
            // {
            //     return (i > 10);
            // };

            //func keyword
            //Func<int,bool> pointer = delegate (int i)
            // {
            //     return (i > 10);
            // };

            //lambda expression
            //Func<int, bool> pointer =  (i) =>
            //  {
            //      return (i > 10);
            //  };
            
            //stage 1 create tree
            Expression< Func<int,bool>> tree = (i) =>  (i > 10);

            //step 2 compile tree
           Func<int,bool>  pointer = tree.Compile();






            Stopwatch watch = new Stopwatch();
            watch.Start();

            //bool result = Chake(100);
            // bool result = pointer(100);
            bool result = pointer(100);



            watch.Stop();

            Console.WriteLine("time taken " + watch.ElapsedTicks.ToString());
            Console.WriteLine("result is" + result);
            Console.ReadLine();
        
        }

        public static bool Chake(int i)
        {
            return (i > 10);
        }
    }

    
}
