﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prop1
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Extended class
            //Console.WriteLine("enter some data");
            //string sample = Console.ReadLine();

            //// MyUtilityClass m1 = new MyUtilityClass();


            ////Console.WriteLine(m1.ChakeForEmail(sample));

            ////call as per static class
            //// bool result = MyUtilityClass.ChakeForEmail(sample);
            ////call on sample 

            //bool result1 = sample.ChakeForEmail();
            //Console.WriteLine(result1);
            //Console.ReadLine();



            //int[] arr = new int[] {10,25,30,665};

            //Console.WriteLine(arr.Average());


            //var v = new { No = 1, Name = "rehan", Address = "pune" };


            //Console.WriteLine(v.Name.ChakeForEmail()); 

            #endregion




        }
    }


    public static class MyUtilityClass
    {
        public static bool ChakeForEmail <T>(this T str)
        {
            return true;
        }

        public static bool ChakeForEmail (this string src)
        {
            return src.Contains("@");
        }
    }
    
    #region Sealed
    //public sealed class A
    //{
    //    void Add() { }
    //}

    //public class B : A
    //{
    //    void Sub() { }
    //} 
    #endregion

}
