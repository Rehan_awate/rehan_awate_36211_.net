﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Partial method demo
            //Emp e = new Emp();
            //e.Age = 23; 
            #endregion
            //Factory factory = new Factory();
            //Console.WriteLine("enter choice");
            //int ch = Convert.ToInt32(Console.ReadLine());

            //dynamic obj = factory.getMeSomeObject(ch);
            //Console.WriteLine(obj.GetDetails());

            Player player = new Player();

            Console.WriteLine(player.GetDetails(10, name : "kolkata"));

        }
    }

    public class Player
    {
        public string GetDetails(int no,string name="alfan",string address="pune")
        {
            return string.Format(" Detilas are : no {0} name  {1}  address{2} ",no,name,address);
        }
    }
    public class Book
    {
        public string GetDetails()
        {
            return "some Book Details";
        }
    }

    public class Factory
    {
        public object getMeSomeObject(int ch)
        {
            if(ch == 1)
            {
                return new Emp();

            }

            else
            {
                return new Book();
            }
        }
      
    }
}
