﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Customer1
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("Enter the Assembly Path : exe /dll built with .NET");
            string pathOfAssembly = Console.ReadLine();

            // @C:\rehan_awate_36211_.net\Classwork\9_oct\Solution2\MathsLib\bin\Debug\MathsLib.dll";

            //below code is for loading top level TYPE metadata information from MathsLib.dll
            Assembly assembly = Assembly.LoadFrom(pathOfAssembly);

            //Now we ask assembly object rest of the details....

            Type[] allTypes = assembly.GetTypes();

            foreach (Type type in allTypes)
            {
                Console.WriteLine("Type: " + type.Name);

                MethodInfo[] allMethods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MethodInfo method in allMethods)
                {
                    Console.Write(method.ReturnType + "  " + method.Name + "  ( ");

                    ParameterInfo[] allParams = method.GetParameters();

                    foreach (ParameterInfo para in allParams)
                    {
                        Console.Write(para.ParameterType.ToString() + "  " + para.Name + "  ");
                    }

                    Console.Write(" ) ");
                    Console.WriteLine();

                    object[] allParas = new object[] { 10, 20 };
                    object result = type.InvokeMember(method.Name,
                                        BindingFlags.Public |
                                        BindingFlags.Instance |
                                        BindingFlags.InvokeMethod,
                                        null, dynamicObject, allParas);

                    Console.WriteLine("Result of " + method.Name + " executed is " + result.ToString());

                }

            }

        }
    }
}
