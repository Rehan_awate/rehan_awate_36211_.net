﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter choice 1.Pdf 2.Word 3.Excel 4.Txt");
            int ch = Convert.ToInt32(Console.ReadLine());


            switch (ch)
            {
                case 1:
                    Pdf p1 = new Pdf();
                    p1.Generate();
                    break;
                case 2:
                    Word w1 = new Word();
                    w1.Generate();
                    break;
                case 3:
                    Excel e1 = new Excel();
                    e1.Generate();
                    break;

                case 4:
                    Txt t1 = new Txt();
                    t1.Generate();
                    break;

                default:
                    Console.WriteLine("invalid");
                    break;
            }
            Console.ReadLine();
        }
        
    }

    public abstract class Report {

        public abstract void Parse();
        protected abstract void Validate();
        protected abstract void Save();

        public virtual void Generate()
        {
            Parse();
            Validate();
            Save();
        }
    } 

    public class Pdf : Report
    {

        protected override void Parse()
        {
            Console.WriteLine("parse in Pdf");
        }
        protected override void Validate()
        {
            Console.WriteLine("Validate in Pdf");
        }
        protected override void Save()
        {

            Console.WriteLine("save in pdf");
        }



    }
    public class Word : Report
    {

        protected override void Parse()
        {
            Console.WriteLine("parse in word");
        }
        protected override void Validate()
        {
            Console.WriteLine("Validate in word");
        }
        protected override void Save()
        {

            Console.WriteLine("save in word");
        }




    }

    public class Excel : Report
    {
        protected override void Parse()
        {
            Console.WriteLine("parse in excel");
        }
        protected override void Validate()
        {
            Console.WriteLine("Validate in excel");
        }
        protected override void Save()
        {
            Console.WriteLine("save in excel");
        }



    }

    public class Txt : Report
    {
        protected override void Parse()
        {
            Console.WriteLine("parse in Txt");
        }
        protected override void Validate()
        {
            Console.WriteLine("Validate in Txt");
        }

        protected void Revalidate()
        {
            Console.WriteLine("Revalidate in Txt");
        }
        protected override void Save()
        {
            Console.WriteLine("save in Txt");
        }

     public override void Generate()
        {
            Parse();
            Validate();
            Revalidate();
            Save();
        }





    }

}
