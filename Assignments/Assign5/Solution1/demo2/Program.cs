﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyLoggerLib;


namespace demo2
{
    class Program
    {
        static void Main(string[] args)

        {

            Console.WriteLine("enter choice 1.Sql 2.Oracle");
            int ch = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("enter choice 1.Insert 2.update 3. Delete");
            int opch = Convert.ToInt32(Console.ReadLine());

            ObjFact obj1 = new ObjFact();
            Databases db = obj1.GetMeSomeObj(ch);

            switch (opch)
            {
                case 1:
                    db.Insert();
                    

                    break;
                case 2:
                    db.Update();

                    break;
                case 3:
                    db.Delete();

                    break;
                default:
                    break;
            }
        }
    }

    public class ObjFact
    {
        public Databases GetMeSomeObj(int ch)

        {
            if (ch == 1)
                return new Sql();
            else return new Oracle();
        }

    }

   public abstract class  Databases
    {
        public abstract void Insert();
        public abstract void Update();
        public abstract void Delete();
    }
   
    public class Sql : Databases
    {
        public override void Insert()

        {



            Console.WriteLine("Insert into sql");
        }
        public override void Update()
        {
            Console.WriteLine("Update into sql");
        }
        public override void Delete()
        {
            Console.WriteLine("Delete into sql");
        }
    }

    public class Oracle :  Databases
  {
        public override void Insert()
        {
            Console.WriteLine("Insert into Oracle");
        }
        public override void Update()
        {
            Console.WriteLine("Update into Oracle");
        }
        public override void Delete()
        {
            Console.WriteLine("Delete into Oracle");
        }
    }
}
