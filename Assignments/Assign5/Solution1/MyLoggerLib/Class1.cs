﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace MyLoggerLib
{
    class MyLoggerLib
    {
        private static MyLoggerLib logger = new MyLoggerLib();


        private MyLoggerLib()
        {
            Console.WriteLine("logger created");
        }

        public static MyLoggerLib CurrentLogger
        {
            get { return logger; }

        }

        public void PrintDetails(string msg)
        {
            Console.WriteLine(msg);
        }


    }
}
