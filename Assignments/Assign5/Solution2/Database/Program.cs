﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerLib;

namespace Database
{
    class Program
    {
        static void Main(string[] args)

        {


            



            Console.WriteLine("enter choice 1.Sql 2.Oracle");
            int ch = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("enter choice 1.Insert 2.update 3. Delete");
            int opch = Convert.ToInt32(Console.ReadLine());

            ObjFact obj1 = new ObjFact();
            Databases db = obj1.GetMeSomeObj(ch);

          // ILogger obj = new Logger();

            switch (opch)
            {
                case 1:
                    db.Insert();


                    break;
                case 2:
                    db.Update();

                    break;
                case 3:
                    db.Delete();

                    break;
                default:
                    break;
            }
        }
    }

    public class ObjFact
    {
        public Databases GetMeSomeObj(int ch)

        {
            if (ch == 1)
                return new Sql();
            else return new Oracle();
        }

    }

    public abstract class Databases
    {
        public abstract void Insert();
        public abstract void Update();
        public abstract void Delete();
    }

    public class Sql : Databases
    {
       
        
        public override void Insert()

        {
            
            
            //obj.Log("insert by sql");
            
            Logger.CurrentLogger.Log("insert by sql");

            Console.WriteLine("Insert into sql");
        }
        public override void Update()
        {
            //obj.Log("update by sql");
           Logger.CurrentLogger.Log("update by sql");
            Console.WriteLine("Update into sql");
        }
        public override void Delete()
        {
            //obj.Log("delete by sql");
            Logger.CurrentLogger.Log("delete by sql");
            Console.WriteLine("Delete into sql");
        }
    }

    public class Oracle : Databases
    {
        //ILogger obj = new Logger();
        public override void Insert()
        {
            //obj.
            //obj.Log("insert by oracle");
           Logger.CurrentLogger.Log("insert by oracle");
            Console.WriteLine("Insert into Oracle");
        }
        public override void Update()
        {
           // obj.Log("update by oracle");
            Logger.CurrentLogger.Log("update by oracle");
            Console.WriteLine("Update into Oracle");
        }
        public override void Delete()
        {
            //obj.
            //obj.Log("delete by oracle");
            Logger.CurrentLogger.Log("delete by oracle");
            Console.WriteLine("Delete into Oracle");
        }
    }
}
