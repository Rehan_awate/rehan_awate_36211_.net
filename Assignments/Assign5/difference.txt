Diiference


Class	

1.A class describes the attributes and behaviors of an object.
2.A class may contain abstract methods, concrete methods.
3.Members of a class can be public, private, protected or default.

Interface

1.An interface contains only abstract methods.
2.An interface contains behaviors that a class implements.
3.All the members of the interface are public by default.