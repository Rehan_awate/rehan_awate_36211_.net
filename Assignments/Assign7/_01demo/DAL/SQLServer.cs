﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using _01demo.POCO;

namespace _01demo.DAL
{
    class SQLServer : IDatabase
    {
        public List<Emp> Select()
        {
            List<Emp> allEmployees = new List<Emp>();
            string constr = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(constr);

            SqlCommand cmd = new SqlCommand("select * from Emp ", con);

            con.Open();

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Emp emp = new Emp()
                {

                
                  No = Convert.ToInt32(reader["No"]),
                    Name = reader["Name"].ToString(),
                    Address = reader["Address"].ToString()
                 };

               allEmployees.Add(emp);
             }
               con.Close();
            return allEmployees;
        }
        public int Insert(Emp emp)
        {
            string constr = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(constr);
            string queryFormat = "insert into employee values ( {0},'{1}','{2}')";
            string finalQuery = string.Format(queryFormat, emp.No, emp.Name, emp.Address);

            SqlCommand cmd = new SqlCommand(finalQuery,con);

            con.Open();

            int rowaffected = cmd.ExecuteNonQuery();

            con.Close();

            return rowaffected;
        }
        public int Update(Emp emp)
        {
            string constr = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(constr);
            string queryFormat = "update employee set   '{1}' = 'shubham' where {0}=10)";
            string finalQuery = string.Format(queryFormat, emp.No, emp.Name);

            SqlCommand cmd = new SqlCommand(finalQuery, con);

            con.Open();

            int rowaffected = cmd.ExecuteNonQuery();

            con.Close();

            return rowaffected;
        }

        public int Delete(Emp emp)
        {
            string constr = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(constr);
            string queryFormat = "delete from employee  where {0}=10)";
            string finalQuery = string.Format(queryFormat, emp.No);

            SqlCommand cmd = new SqlCommand(finalQuery, con);

            con.Open();

            int rowaffected = cmd.ExecuteNonQuery();

            con.Close();

            return rowaffected;
        }

    }
}
