﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _01demo.DAL;
using _01demo.POCO;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace _01demo
{
    class Program
    {
        static void Main(string[] args)
        {
            DBFactory dBFactory = new DBFactory();
            IDatabase db = dBFactory.GetDatabase();

            Console.WriteLine(" 1.insert; 2.update; 3.Detele; ");

            int opChoice = Convert.ToInt32(Console.ReadLine());

            switch (opChoice)
            {
               
                case 1:

                    SqlConnection con = null;

                    try
                    {
                        string constr = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();
                        con = new SqlConnection(constr);

                        SqlCommand cmd = new SqlCommand("spInsert1", con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter parameter1 = new SqlParameter("@no", SqlDbType.Int);
                        Console.WriteLine("enter no");
                        parameter1.Value = Convert.ToInt32(Console.ReadLine());

                        SqlParameter parameter2 = new SqlParameter("@name", SqlDbType.VarChar, 50);
                        Console.WriteLine("enter name");
                        parameter2.Value = Console.ReadLine();

                        SqlParameter parameter3 = new SqlParameter("@address", SqlDbType.VarChar, 50);
                        Console.WriteLine("enter address");
                        parameter3.Value = Console.ReadLine();

                        cmd.Parameters.Add(parameter1);
                        cmd.Parameters.Add(parameter2);
                        cmd.Parameters.Add(parameter3);

                        con.Open();

                        cmd.ExecuteNonQuery();

                        Console.WriteLine("record inserted");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("record insertion failed");
                        Console.WriteLine(ex.Message);

                    }
                    finally
                    {
                        con.Close();
                    }






                   
                    break;

                case 2:
                    SqlConnection con1 = null;

                    try
                    {
                        string constr = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();
                        con = new SqlConnection(constr);

                        SqlCommand cmd = new SqlCommand("spUpdate", con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter parameter1 = new SqlParameter("@no", SqlDbType.Int);
                        Console.WriteLine("enter no");
                        parameter1.Value = Convert.ToInt32(Console.ReadLine());

                        SqlParameter parameter2 = new SqlParameter("@name", SqlDbType.VarChar, 50);
                        Console.WriteLine("enter name");
                        parameter2.Value = Console.ReadLine();

                      

                        cmd.Parameters.Add(parameter1);
                        cmd.Parameters.Add(parameter2);
                      

                        con.Open();

                        cmd.ExecuteNonQuery();

                        Console.WriteLine("record updated");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("record update failed");
                        Console.WriteLine(ex.Message);

                    }
                    finally
                    {
                        con1.Close();
                    }

                    break;


                case 3:

                    SqlConnection con3 = null;
                    try
                    {
                        con = new SqlConnection(@"Server = (LocalDB)\MSSQLLocalDB; database=Study; Integrated Security = true");
                        con.Open();


                        Console.WriteLine("enter name");
                        string Name1 = Console.ReadLine();



                        string queryTeemplate = "delete from  Employee    where Name ='{0}' ";
                        string finalInsertQuery = string.Format(queryTeemplate, Name1);

                        SqlCommand cmd = new SqlCommand(finalInsertQuery, con);

                        int NoOfRowAffected = cmd.ExecuteNonQuery();

                        Console.WriteLine("No of rows affected=" + NoOfRowAffected.ToString());





                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("something is not right");
                        Console.WriteLine("Technical msg" + ex.Message);

                    }
                    finally
                    {
                        con3.Close();
                    }
                    Console.ReadLine();

                    break;



            }



        }
    }
}
