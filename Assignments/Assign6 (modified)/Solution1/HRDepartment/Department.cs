﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRDepartment
{
    public class Department
    {
        #region Private Datamembers
        private int _deptNo;
        private string _deptName;
        private string _location;

         #endregion
        
        #region Constructor
        public Department()
        {
            DeptNo = 0;
            DeptName = "";
            Location = "";
        }
        public Department(int deptno, string deptname, string location)
        {
            DeptNo = deptno;
            DeptName = deptname;
            Location = location;
        }
        #endregion

        #region Getter and setter

        public int DeptNo { get => _deptNo; set => _deptNo = value; }
        public string DeptName { get => _deptName; set => _deptName = value; }
        public string Location { get => _location; set => _location = value; }
        #endregion
        
        #region member function
        public override string ToString()
        {
            return "DeptNo: " + DeptNo + " DeptName:  " + DeptName + " Location: " + Location;
        }
        #endregion
    }
}
