﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRDepartment
{
    class Employee
    {
        #region Private Data Members
        private int _empNo;
        private string _name;
        private string _designaion;
        private double _salary;
        private double _commision;
        private int _deptNo;
        #endregion
        
        #region Constructors
        public Employee()
        {
            EmpNo = 0;
            Name = "";
            Designaion = " ";
            Salary = 0;
            Commision = 0;
            DeptNo = 0;
        }
        public Employee(int empno, string name, string designation, double salary, double commision, int deptno)
        {
            EmpNo = empno;
            Name = name;
            Designaion = designation;
            Salary = salary;
            Commision = commision;
            DeptNo = deptno;
        }
        #endregion
        
        #region Getter and setter
        public int EmpNo { get => _empNo; set => _empNo = value; }
        public string Name { get => _name; set => _name = value; }
        public string Designaion { get => _designaion; set => _designaion = value; }
        public double Salary { get => _salary; set => _salary = value; }
        public double Commision { get => _commision; set => _commision = value; }
        public int DeptNo { get => _deptNo; set => _deptNo = value; }
        #endregion
        
        #region Member Function
        public override string ToString()
        {
            return "EmpNo: " + EmpNo + " Name: " + Name + " Designation: " + Designaion + " Salary: " + Salary + " Commision: " + Commision + " Deptno: " + DeptNo;
        }
        #endregion
    
}
}
