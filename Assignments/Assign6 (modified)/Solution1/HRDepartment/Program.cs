﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HRDepartment
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Object Created Multiple
            List<string> locations = new List<string>();
            List<int> count = new List<int>();
            List<string> arr = new List<string>();
            List<string> arr1 = new List<string>();
            List<string> listOfLocation = new List<string>();
            Dictionary<int, Department> DeptList = new Dictionary<int, Department>();
            Dictionary<string, string> obj = new Dictionary<string, string>();
            Dictionary<int, Employee> EmpList = new Dictionary<int, Employee>();
            List<Employee> listEmployee = new List<Employee>();
            Dictionary<int, int> countOfEmployeeDeptwise = new Dictionary<int, int>();
            Dictionary<int, int> countOfEmployeeDeptwise1 = new Dictionary<int, int>();
            Dictionary<int, double> departmentWisevgSalary = new Dictionary<int, double>();
            Dictionary<int, double> departmentWisevgSalary1 = new Dictionary<int, double>();
            Dictionary<int, double> departmentWithMinSalary = new Dictionary<int, double>();
            Dictionary<int, double> departmentWithMinSalary1 = new Dictionary<int, double>();
            #endregion


            #region DepartmentList Display
            // Open file and Read line by line  
            FileStream fs = new FileStream(@"C:\rehan_awate_36211_.net\Assignments\Assign6 (modified)\Solution1\dept.csv", FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(fs);
            string deptstring;
            while ((deptstring = reader.ReadLine()) != null)
            {  //line fetched from csv file  //string deptstring = "50,Training,Karad";   
               // Split details separated by a comma followed by space  
                string[] deptDetails = deptstring.Split(',');
                Department dept = new Department();
                dept.DeptNo = int.Parse(deptDetails[0]);
                dept.DeptName = deptDetails[1];
                dept.Location = deptDetails[2];
                //add dept object in DeptList  

                DeptList.Add(dept.DeptNo, dept);
            }
            foreach (int key in DeptList.Keys)
            {

                Console.WriteLine(DeptList[key].ToString());
            }
            reader.Close();
            fs.Close();
            #endregion

            Console.WriteLine("==================================================");
            #region EmployeeList Display
            FileStream fs1 = new FileStream(@"C:\rehan_awate_36211_.net\Assignments\Assign6 (modified)\Solution1\emp.csv", FileMode.Open, FileAccess.Read);
            StreamReader reader1 = new StreamReader(fs1);
            string empstring;
            while ((empstring = reader1.ReadLine()) != null)
            {  //line fetched from csv file  //string deptstring = "50,Training,Karad";   
               // Split details separated by a comma followed by space  
                string[] empDetails = empstring.Split(',');
                Employee emp = new Employee();
                emp.EmpNo = int.Parse(empDetails[0]);
                emp.Name = empDetails[1];
                emp.Designaion = empDetails[2];
                emp.Salary = double.Parse(empDetails[3]);
                emp.Commision = double.Parse(empDetails[4]);
                emp.DeptNo = int.Parse(empDetails[5]);
                //add dept object in DeptList  

                EmpList.Add(emp.EmpNo, emp);
            }
            /*  foreach (int key in EmpList.Keys)
              {

                  Console.WriteLine(EmpList[key].ToString());
              }*/
            reader1.Close();
            fs1.Close();
            #endregion


            #region 4th Question
            List<string> ListOfLocation = getLocation(DeptList)
; List<string> getLocation(Dictionary<int, Department> DeptList1)
            {
                foreach (int key in DeptList1.Keys)
                {
                    listOfLocation.Add(DeptList1[key].Location);
                }
                return listOfLocation;
            }

            /*    var result = from location in ListOfLocation.GroupBy(x => x)
                             where location.Count() == 1
                             select location.First();*/
            var result = (from dept in DeptList.Values group dept by dept.Location into d where d.Count() == 1 select new { location = d.Key }).ToList();
            foreach (var item in result)
            {
                Console.WriteLine(item.location);
            }

            #endregion

            #region 5th Question
            /*   var col = from c in customers join o in orders on c.CustomerID equals o.CustomerID select new { c.CustomerID, c.Name, o.OrderID, o.Cost };*/
            /*   List<Employee> emps = new List<Employee>();
               foreach (int key in EmpList.Keys)
               {
                   emps.Add(EmpList[key]);
               }
               List<Department> depts = new List<Department>();
               foreach (int key in DeptList.Keys)
               {
                   depts.Add(DeptList[key]);
               }*/

            /*   var res = depts.Where(d => emps.All(e2 => e2.DeptNo != d.DeptNo));*/

            var res = (from dept in DeptList.Values join emp in EmpList.Values on dept.DeptNo equals emp.DeptNo into data where data.Count() == 0 select dept).ToList();


            foreach (Department dept in res)
            {
                Console.WriteLine(dept.DeptName);
            }


            #endregion

            #region 6th Question
            /*    var res1 = (from emp in emps select emp).ToList();*/
            var res1 = (from emp in EmpList.Values select emp.Salary + emp.Commision).Sum();
            Console.WriteLine("TotalSal: " + res1);
            /*   var res1 = (from emp in EmpList.Values select new { emp.Salary, emp.Commision }).ToList();
               double total = 0;
               foreach (var emp in res1)
               {
                   total += emp.Salary+emp.Commision;
               }
               Console.WriteLine("Total salary: "+total);*/

            #endregion


            #region 7thQuestion
            Console.WriteLine("Enter DeptNo: ");
            int deptNo = Convert.ToInt32(Console.ReadLine());
            var res2 = (from emp in EmpList.Values where emp.DeptNo == deptNo select emp).ToList();
            foreach (Employee emp in res2)
            {
                Console.WriteLine(emp);
            }

            #endregion

            #region 8th Question
            var res3 = (from emp in EmpList.Values group emp by emp.DeptNo into g select g).ToDictionary(g => g.Key, g => g.Count());
            foreach (int key in res3.Keys)
            {
                Console.WriteLine(key + " " + res3[key]);
            }
            /*  var res3 = (from emp in EmpList.Values group emp by emp.DeptNo into g select new { deptno = g.Key, Count = g.Count() });*/
            /*foreach (var item in res3)
            {
                Console.WriteLine(item.deptno+" "+item.Count);
            }*/
            #endregion
           
            
            #region 9th Question
            var res4 = (from emp in EmpList.Values group emp by emp.DeptNo into g select g).ToDictionary(g => g.Key, g => g.Average(emp => emp.Salary));
            foreach (KeyValuePair<int, double> item in res4)
            {
                Console.WriteLine("DeptNo: " + item.Key + " " + "AVG Salary " + item.Value);
            }
            /*     foreach (int key in res4.Keys)
                 {
                     Console.WriteLine("DeptNo: "+key+" "+"Average Salary: "+res4[key]);
                 }*/
            #endregion


            #region 10th Question
            /*      var res5 = (from emp in EmpList.Values orderby emp.Salary group emp by emp.DeptNo into g select g.First()).ToDictionary(g => g.DeptNo, g => g.Salary);*/
            var res5 = (from emp in EmpList.Values group emp by emp.DeptNo into g select g).ToDictionary(g => g.Key, g => g.Min(emp => emp.Salary));
            foreach (KeyValuePair<int, double> item in res5)
            {
                Console.WriteLine("DeptNo: " + item.Key + " " + "MIN Salary " + item.Value);
            }
            #endregion

            
            #region 11th Question

            var res6 = (from emp in EmpList
                        join dept in DeptList
                         on emp.Value.DeptNo equals dept.Value.DeptNo
                        where emp.Value.DeptNo == dept.Value.DeptNo
                        select new
                        {
                            Eno = emp.Key,
                            Ename = emp.Value,
                            Deptname = dept.Value.DeptName
                        }).ToDictionary(x => x.Eno, x => String.Concat(x.Ename, ",", x.Deptname));

            foreach (KeyValuePair<int, string> index in res6)
            {
                string[] name = index.Value.Split(',');
                Console.WriteLine("Emp Number = " + index.Key + ", Emp Name = " + name[0] + ", DeptName = " + name[1]);
            }
            #endregion
            Console.ReadLine();
        }

















    }
 }

