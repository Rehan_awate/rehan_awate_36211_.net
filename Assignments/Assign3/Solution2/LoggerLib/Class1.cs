﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerLib
{
    public class Logger
    {
        public static Logger _logger = new Logger();

        private Logger()
        {
            Console.WriteLine(" logger created ");
        }

        public static Logger CurrentLogger
        {
            get { return _logger; }

        }
        public void Log(string msg)
        {
            Console.WriteLine(" Logged " +
                msg);
        }

    }
}
