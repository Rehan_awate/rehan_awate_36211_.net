﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace Q1
{
    class Program
    {
        static void Main(string[] args)
        {
            Book b1 = new Book();
            b1.AcceptDetails();
            b1.PrintDetails();
        }
    }
    struct Book {

        private string title; private bool outofstock; private string author;
        private int isbn; private char index; private double price;


     

        public Book(string title, bool outofstock, string author,
       int isbn, char index, double price)
        {
            this.title = title;

            this.outofstock = outofstock;
            this.author = author;
            this.isbn = isbn;
            this.index = index;
            this.price = price;
        }



        //getter Methods
        public string GetTitle()
        {
            return title;
        }
        public bool Getoutofstock()
        {
            return outofstock;
        }
        public string Getauthor()
        {
            return author;
        }
        public int Getisbn()
        {
            return isbn;
        }
        public char Getindex()
        {
            return index;
        }
        public double Getprice()
        {
            return price;
        }

        //setter Methods
        public void SetTitle(string title)
        {
             this.title = title;

           
        }
        public void Setoutofstock(bool outofstock)
        {
             this.outofstock = outofstock ;
            
        }
        public void Setauthor(string author)
        {
              this.author = author;
           
        }
        public void Setisbn(int isbn)
        {
             this.isbn = isbn;
           
        }
        public void Setindex(char index)
        {
             this.index = index;
           
        }
        public void Setprice(double price)
        {
             this.price = price ;
        }

        public void AcceptDetails()
        {
            Console.WriteLine("enter title");
            SetTitle(Console.ReadLine());

            Console.WriteLine("Is this outofstock");
            Setoutofstock(Convert.ToBoolean(Console.ReadLine()));
           
            Console.WriteLine("enter Auther");
            Setauthor(Convert.ToString(Console.ReadLine()));

            Console.WriteLine("enter  isbn");
            Setisbn(Convert.ToInt32(Console.ReadLine()));

            Console.WriteLine("enter  index");
            Setindex(Convert.ToChar(Console.ReadLine()));

            Console.WriteLine("enter  Price");
            Setprice(Convert.ToDouble(Console.ReadLine()));

           

        }

        public void PrintDetails()
        {
            Console.Write("Title:");
           
            Console.WriteLine(GetTitle());

            Console.Write("isoutofstock:");
            Console.WriteLine( Getoutofstock());

            Console.Write("Auther:");
            Console.WriteLine(Getauthor());

            Console.Write("isbn:");
            Console.WriteLine( Getisbn());

            Console.Write("index:");
            Console.WriteLine( Getindex());

            Console.Write("price:");
            Console.WriteLine( Getprice());

        }

    }

}
