﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicNS
{
    public class BasicCalculator
    {
        public int add(int a, int b)
        {
            return a + b;
        }

        public int sub(int a, int b)
        {
            return a - b;
        }

        public int mul(int a, int b)
        {
            return a * b;
        }

        public int div(int a, int b)
        {
            return a / b;
        }
    }
}

namespace TempratureNS
{
    public class TempratureConverter
    {
        public int FarenheitToCelcius(int a)
        {
           
           
            return ((a - 32) * 5 / 9); ;
        }

        public int CelciusToFarenheit(int a)
        {
           
            return (a * 9 / 5) + 32; ;
        }

        
    }
}
