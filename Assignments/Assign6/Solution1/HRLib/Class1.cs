﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRLib
{
    public class Employee
    {
        private int _EmpNo;
        private string _Name;
        private string _Designation;
        private double _Salary;
        private double _Commision;
        private int _DeptNO;


        public int DeptNO
        {
            get { return _DeptNO; }
            set { _DeptNO = value; }
        }


        public double Commision
        {
            get { return _Commision; }
            set { _Commision = value; }
        }


        public double Salary
        {
            get { return _Salary; }
            set { _Salary = value; }
        }


        public string Designation
        {
            get { return _Designation; }
            set { _Designation = value; }
        }


        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int EmpNo
        {
            get { return _EmpNo; }
            set { _EmpNo = value; }
        }

        public override string ToString()
        {
            return " DeptNo " + this._DeptNO + " EmpNO " + this._EmpNo +
                " name " + this._Name + " salary " + this._Salary + "designation" + this._Designation
                + "commision" + this._Commision;

        }

    }

    public class Department
    {
        private int _DeptNO;
        private string _DName;
        private string _Location;

        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }


        public string DName
        {
            get { return _DName; }
            set { _DName = value; }
        }


        public int DeptNO
        {
            get { return _DeptNO; }
            set { _DeptNO = value; }
        }

        public override string ToString()
        {
            return " DeptNo " + this._DeptNO + " DName " + this._DName + " location " + this._Location;

        }
    }
}
