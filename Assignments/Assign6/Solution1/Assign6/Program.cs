﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using HRLib;

namespace Assign6
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Dept details from dept.csv

            FileStream fs = new FileStream(@"C:\rehan_awate_36211_.net\Assignments\Assign6\Solution1\dept.csv", FileMode.Open, FileAccess.Read);

            StreamReader reader = new StreamReader(fs);

            Dictionary<int, Department> DeptList = new Dictionary<int, Department>();

            string deptstring;
            while ((deptstring = reader.ReadLine()) != null)
            {
                
                string[] deptDetails = deptstring.Split(',');
                Department dept = new Department();
                dept.DeptNO = int.Parse(deptDetails[0]);
                dept.DName = deptDetails[1];
                dept.Location = deptDetails[2];
                DeptList.Add(dept.DeptNO, dept);
            }

            Console.WriteLine("---Department info--");

            foreach (KeyValuePair<int, Department> d in DeptList)
            {
                Console.WriteLine(d.ToString());
            }

            #endregion

            #region Employee Details From emp.csv

            FileStream fs1 = new FileStream(@"C:\rehan_awate_36211_.net\Assignments\Assign6\Solution1\emp.csv", FileMode.Open, FileAccess.Read);

            StreamReader reader1 = new StreamReader(fs1);

            Dictionary<int, Employee> EmpList = new Dictionary<int, Employee>();

            string empstring;
            while ((empstring = reader1.ReadLine()) != null)
            {
                string[] empDetails = empstring.Split(',');
                Employee emp = new Employee();
                emp.EmpNo = int.Parse(empDetails[0]);
                emp.Name = empDetails[1];
                emp.Designation = empDetails[2];
                emp.Salary = double.Parse(empDetails[3]);
                emp.Commision = double.Parse(empDetails[4]);
                emp.DeptNO = int.Parse(empDetails[5]);
                EmpList.Add(emp.EmpNo, emp);
            }

            Console.WriteLine("----Employee info-----");

            foreach (KeyValuePair<int, Employee> e in EmpList)
            {
                Console.WriteLine(e.ToString());
            }
            Console.ReadLine();

            fs.Flush();
            fs1.Flush();
            fs.Close();
            fs1.Close();


            #endregion

            #region  LocationWithSingleDept(DeptList)

            List<string> LocationWithSingleDept(Dictionary<int, Department> deptList)
            {
                List<string> loc = new List<string>();
                foreach (KeyValuePair<int, Department> d in deptList)
                {
                    if (loc.Contains(d.Value.Location))
                    {
                        loc.Remove(d.Value.Location);
                    }
                    else
                    {
                        loc.Add(d.Value.Location);
                    }
                }

                return loc;
            }

            Console.WriteLine(" Location With Single Department");

            List<string> location = new List<string>();
            location = LocationWithSingleDept(DeptList);
            foreach (var l in location)
            {
                Console.WriteLine(l);
            }

            #endregion

            #region FindDeptsWithNoEmps

            List<string> FindDeptsWithNoEmps(Dictionary<int, Employee> empList)
            {
                int test = 0;
                List<string> DeptName = new List<string>();
                foreach (KeyValuePair<int, Department> dept in DeptList)
                {
                    test = 0;
                    foreach (KeyValuePair<int, Employee> emp in empList)
                    {
                        if (dept.Value.DeptNO == emp.Value.DeptNO)
                        {
                            test++;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    if (test == 0)
                    {
                        DeptName.Add(dept.Value.DName);
                    }
                }

                return DeptName;
            }


            Console.WriteLine("-------Department With No Employees----");

            List<string> st = new List<string>();
            st = FindDeptsWithNoEmps(EmpList);
            foreach (var name in st)
            {
                Console.WriteLine(name);
            }

            #endregion

            #region  Calcluate_Total_Salary(EmpList)

            double Calcluate_Total_Salary(Dictionary<int, Employee> empList)
            {
                double total_salary = 0;
                foreach (KeyValuePair<int, Employee> emp in empList)
                {
                    total_salary += emp.Value.Salary + emp.Value.Commision;
                }

                return total_salary;
            }

            Console.WriteLine("------Total salary of Employees----");

            double salary = Calcluate_Total_Salary(EmpList);
            Console.WriteLine("Total Salary of all Employees " + salary);

            #endregion

            #region List<Employee> GetAllEmployeesByDept(int DeptNo)

            List<Employee> GetAllEmployeesByDept(int DeptNo)
            {
                List<Employee> emp = new List<Employee>();
                foreach (KeyValuePair<int, Employee> e in EmpList)
                {
                    if (e.Value.DeptNO == DeptNo)
                    {
                        emp.Add(e.Value);
                    }
                }

                return emp;
            }

            Console.WriteLine("-----All Employees by Department-------");

            List<Employee> employee = new List<Employee>();
            employee = GetAllEmployeesByDept(30);
            foreach (var detail in employee)
            {
                Console.WriteLine(detail.ToString());
            }


            #endregion

            #region DeptwiseStaffCount(EmpList)

            Dictionary<int, int> DeptwiseStaffCount(Dictionary<int, Employee> empList)
            {
                int count = 0;
                Dictionary<int, int> countOfEmployee = new Dictionary<int, int>();
                foreach (KeyValuePair<int, Department> dept in DeptList)
                {
                    count = 0;
                    foreach (KeyValuePair<int, Employee> emp in empList)
                    {
                        if (dept.Value.DeptNO == emp.Value.DeptNO)
                        {
                            count++;
                        }
                    }

                    countOfEmployee.Add(dept.Value.DeptNO, count);
                }

                return countOfEmployee;
            }

            Console.WriteLine("-------Department wise staff Count--------");

            Dictionary<int, int> staffEmp = new Dictionary<int, int>();
            staffEmp = DeptwiseStaffCount(EmpList);
            foreach (KeyValuePair<int, int> e in staffEmp)
            {
                Console.WriteLine(e);
            }

            #endregion

            #region DeptwiseAvgSal(EmpList)

            Dictionary<int, Double> DeptwiseAvgSal(Dictionary<int, Employee> empList)
            {
                double Average_Salary = 0;
                Dictionary<int, double> AvgSalary = new Dictionary<int, double>();
                foreach (KeyValuePair<int, Department> dept in DeptList)
                {
                    Average_Salary = 0;
                    foreach (KeyValuePair<int, Employee> emp in empList)
                    {
                        if (dept.Value.DeptNO == emp.Value.DeptNO)
                        {
                            Average_Salary += emp.Value.Salary + emp.Value.Commision;
                        }
                    }

                    AvgSalary.Add(dept.Value.DeptNO, Average_Salary);
                }

                return AvgSalary;
            }

            Console.WriteLine("-------Department Wise Average Salary-----");

            Dictionary<int, double> sal = new Dictionary<int, double>();
            sal = DeptwiseAvgSal(EmpList);
            foreach (KeyValuePair<int, double> average in sal)
            {
                Console.WriteLine(average);
            }

            #endregion

            #region DeptwiseMinSal(EmpList)


            Dictionary<int, Double> DeptwiseMinSal(Dictionary<int, Employee> empList)
            {
                double minimum_Salary = double.MaxValue;
                Dictionary<int, double> MINSalary = new Dictionary<int, double>();
                List<double> list = new List<double>();
                foreach (KeyValuePair<int, Department> dept in DeptList)
                {
                    list.Clear();
                    foreach (KeyValuePair<int, Employee> emp in empList)
                    {
                        if (dept.Value.DeptNO == emp.Value.DeptNO)
                        {
                            list.Add(emp.Value.Salary);
                        }

                    }

                    minimum_Salary = double.MaxValue;
                    //Console.WriteLine("list");
                    bool isEmpty = !list.Any();
                    if (isEmpty)
                    {
                        //Console.WriteLine("Empty");
                        minimum_Salary = 0.0;
                    }
                    else
                    {
                        //Console.WriteLine("List is not empty");
                    }
                    foreach (double d in list)
                    {
                        //Console.WriteLine(d);
                        if (minimum_Salary > d)
                        {
                            minimum_Salary = d;
                        }


                    }

                    MINSalary.Add(dept.Value.DeptNO, minimum_Salary);
                }

                return MINSalary;
            }

            Console.WriteLine("----Department Wise MIN Salary-----");

            Dictionary<int, double> min_sal = new Dictionary<int, double>();
            min_sal = DeptwiseMinSal(EmpList);
            foreach (KeyValuePair<int, double> s in min_sal)
            {
                Console.WriteLine(s);
            }

            #endregion

            Console.ReadLine();

        }


    }
}
