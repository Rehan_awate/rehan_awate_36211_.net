﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMSLib;

namespace _worker
{
    class Program
    {
        static void Main(string[] args)
        {
           

            string exit = "y";
            do
            {
                Console.WriteLine("Enter Employee Types");
                Console.WriteLine("1.Employee  2 . SalesPerson Employee 3.WageEmployees 4.Department ");
                int choice = Convert.ToInt32(Console.ReadLine());
                Person p = null;

                switch (choice)
                {
                    case 1:
                        p = new Employee();
                        p.AcceptDetails();
                        p.PrintDetails();
                        break;
                    case 2:
                        p = new Salesperson();
                        p.AcceptDetails();
                        p.PrintDetails();
                        break;
                    case 3:
                        p = new WageEmp();
                        p.AcceptDetails();
                        p.PrintDetails();
                        break;
                    case 4:
                        Department d = new Department();
                       d.AcceptDetails();
                        d.PrintDetails();
                        break;
                    default:

                        break;
                }
                Console.WriteLine("do you want to continue enter y /n ");
                exit = Console.ReadLine();
            } while (exit == "y");

            Console.ReadLine();
        }
    }
}
