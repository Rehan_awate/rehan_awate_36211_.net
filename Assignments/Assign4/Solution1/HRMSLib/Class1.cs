﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Date
    {
        private int _day;
        private int _month;
        private int _year;

      public  Date()
        {
            this._day = 1;
            this._month = 1;
            this._year = 2000;
        }

        public Date(int day, int month, int year)
        {
            this._day = day;
            this._month = month;
            this._year = year;
        }

        public int year
        {
            get { return _year; }
            set { 
                if( value <= 2100 && value >= 1900)
                   _year = value;
                else
                    Console.WriteLine("invalid");
                }
          
            
        }


        public int month
        {
            get { return _month; }
            set {
                if(value > 0 && value < 13)
                _month = value;
            else
                    Console.WriteLine("invalid");
            }
        }

        public int day
        {
            get { return _day; }
            set { 
                if( (this._year % 4 == 0) || (this._year % 400 == 0) )
                {
                    if ( _month == 2 )
                    {
                        if (value > 0 && value <= 28)
                            _day = value;
                    }
                    else
                        Console.WriteLine("invalid");
                   
                }
                else
                {
                    if(_month == 1 && _month == 3 && _month == 5 && _month == 7 && _month == 8 && _month == 10 && _month == 12 )
                    {
                        if (day > 0 && day <= 31)
                            _day = value;
                    }
                    else
                       if (day > 0 && day <= 30)
                        _day = value;
                }
                
            
            }
        }

        public override string ToString()
        {
            return this.day + "/" + this.month + "/" + this.year;
        }

        public void AcceptDetails()
        {
            Console.WriteLine(" enter day");
            this.day = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("  enter month");
            this.month = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(" enter year");
            this.year = Convert.ToInt32(Console.ReadLine());

        }

        public void PrintDetails()
        {
            Console.WriteLine("Day is :" + this.day);
            Console.WriteLine("Month is :" + this.month);
            Console.WriteLine("Year is :" + this.year);
        }

        public static int differenceOfYear(Date first, Date second)
        {
            return Math.Abs(first.year - second.year);
        }


    }
   public class Person
    {
        private string _name;
        private string _gender;
        private Date _birth;
        private string _address;
        private string _EmailId;
     

       


        public Person()
        {
            this._name = "";
            this._gender = "";
            this._address = "";
            this._EmailId = "";
            
        }

        public Person(string name, string gender, Date birth,string address,string emailId)
        {
            this._name = name;
            this._gender = gender;
            this._birth = birth;
            this._address = address;
            this._EmailId = emailId;
        }

       

        public string EmailId
        {
            get { return _EmailId; }
            set { _EmailId = value; }
        }
        public string address
        {
            get { return _address; }
            set { _address = value; }

        }


        public Date birth
        {
            get { return _birth; }
            set { _birth = value; }

        }


        public string gender
        {
            get { return _gender; }
            set { _gender = value; }

        }

        public string name
        {
            get { return _name; }
            set { _name = value; }

        }

        public virtual void AcceptDetails()
        {

            Console.WriteLine(" enter Name");
            this.name = Console.ReadLine();
            Console.WriteLine("  enter Gender");
            this.gender = Console.ReadLine();
            Console.WriteLine(" enter Address");
            this.address = Console.ReadLine();

            Console.WriteLine(" enter EmailAddress");
            this.EmailId = Console.ReadLine();

            Console.WriteLine(" enter birth day month year");
            
            Date d = new Date();
            
            this.birth = d;
            this.birth.AcceptDetails();

        }

        public virtual void PrintDetails()
        {
            Date d = new Date();

            d.day = Convert.ToInt32(DateTime.Now.ToString("dd"));
            d.month = Convert.ToInt32(DateTime.Now.ToString("MM"));
            d.year = Convert.ToInt32(DateTime.Now.ToString("yyyy"));
            Console.WriteLine("Name : " + this.name);
            Console.WriteLine("Gender: " + this.gender);
            this.birth.PrintDetails();
            Console.WriteLine("Address  :" + this.address);
            Console.WriteLine("MailId  :" + this.EmailId);
            Console.WriteLine("Age :" + Date.differenceOfYear(this.birth, d));
        }

        public override string ToString()
        {
            Date d = new Date();

            d.day = Convert.ToInt32(DateTime.Now.ToString("dd"));
            d.month = Convert.ToInt32(DateTime.Now.ToString("MM"));
            d.year = Convert.ToInt32(DateTime.Now.ToString("yyyy"));

            return "Name :" + this.name + "\n Gender  :" + this.gender + "\nBirth date  :" + this.birth.ToString() + "\nAddress is :" + this.address + "\n Email id " + this.EmailId +  "\n Age is" + Date.differenceOfYear(d, this.birth).ToString();
        }

        public void Age()
        {
            Console.WriteLine("birth calculated");
        }

    }

    enum EmployeeTypes
    {
        Trainee =1, Permanent, Temporary
    }

   public  class Employee : Person
    {
        private static int _id;
        private double _salary ;

        private double _hra;
        private double _da;
        private string _designation;
        private string _Passcode;
        private int _DeptNo;
        private Date _Hiredate;
        private EmployeeTypes _EmpTypes;

        public Employee()
        {
            Employee._id++;
            this.id = Employee._id;
            this.salary = 0;
            this.hra = 0;
            this.da = 0;
            this.designation = "";
            this._EmpTypes = 0;
            this.Passcode = "";
            this._DeptNo = 0;

        }


        public Employee(string name, string gender,
            Date birth, string address, string emailId, double salary,
            double hra, double da, string designation) 
            : base(name,gender,birth,address,emailId)
        { 
            this.gender = gender;
            this.birth = birth;
            this.address = address;
            this.salary = salary;
            this.hra = hra;
            this.da = da;
            this.designation = designation;
            
        }



    public Date Hiredate
        {
            get { return _Hiredate; }
            set { _Hiredate = value; }
        }


        public int Deptno
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }


        public string Passcode
        {
            get { return _Passcode; }
            set { _Passcode = value; }
        }

      

       

        public string designation
        {
            get { return _designation; }
            set { _designation = value; }

        }


        public double da
        {
            get { return _da; }
            set { _da = value; }

        }


        public double hra
        {
            get { return _hra; }
            set { _hra = value; }

        }


        public double salary
        {
            get { return _salary; }
            set { _salary = value; }

        }

        public int id
        {
            get;
            private set;
            
        }

        public override void AcceptDetails()
        {
            base.AcceptDetails();
            Console.WriteLine(" enter salary");
            this.salary = Convert.ToDouble(Console.ReadLine());

            this.hra = (this.salary * 4) / 10;
            
            this.da = this.salary / 10;
            
            Console.WriteLine(" enter designation");
            this.designation = Console.ReadLine();
            
            Console.WriteLine(" enter Trainee or Temporary or  Permanent");
            string s = Console.ReadLine();

            if (s == "Trainee")
            {
                this._EmpTypes = EmployeeTypes.Trainee;
            }
            else if (s == "Temporary")
            {
                this._EmpTypes = EmployeeTypes.Temporary;
            }
            else if (s == "Permanent")
            {
                this._EmpTypes = EmployeeTypes.Permanent;
            }
            else
            {
                this._EmpTypes = 0;
            }

        }
        public override void PrintDetails()
        {

            base.PrintDetails();
            Console.WriteLine("Employee id  :" + this.id);
            Console.WriteLine("Net Salary  :" + this.NetSalary());
            
            Console.WriteLine(this.designation);
            Console.WriteLine(this._EmpTypes);
            Console.WriteLine(this.Passcode);
            Console.WriteLine(this.Deptno);
            Console.WriteLine(this.Hiredate);

        
        }

        public virtual double NetSalary()
        {
            return this.salary + this.da + this.hra;
        }
        public override string ToString()
        {
            return base.ToString() + "\n salary :" +
                this.salary.ToString() + "\n Hra :" + this.hra.ToString()
                + "\n da :" + this.da.ToString() + "\n Designation :"
                + this.designation + " \n employee type :" + this._EmpTypes.ToString()
                + "\n passcode" + this.Passcode + "dept no" + this.Deptno + "date " + this.Hiredate;
        }

    }

    public class Salesperson : Employee
    {
        private double _commision;

        public Salesperson()
        {
            this._commision = 0;
        }

        public Salesperson(string name, string gender, Date birth, 
            string address, string emailId, double salary, double hra,
            double da, string designation,
            double commision) : 
            base(name, gender, birth, address, emailId,
                salary, hra, da, designation)
        {
            this.commision = commision;
        }

       

        public double commision
        {
            get { return _commision; }
            set { _commision = value; }
        }

        public override string ToString()
        {
            return base.ToString() + "\n Commision :" + this.commision;
        }
        public override void AcceptDetails()
        {
            base.AcceptDetails();
            Console.WriteLine(" enter commision");
            this.commision = Convert.ToDouble(Console.ReadLine());
        }
        public override void PrintDetails()
        {
            base.PrintDetails();
            Console.WriteLine(this.commision);
        }
        public override double NetSalary()
        {
            return base.NetSalary() + this.commision;
        }




    }
   public class WageEmp : Employee
    {
        private int _hours;

        private int _rate;

       public WageEmp()
        {
            this._hours = 0;
            this._rate = 0;
        }

        public WageEmp(string name, string gender, Date birth, string address, string emailId, double salary,
            double hra, double da, string designation, int hour, int rate)
            : base(name, gender, birth, address,emailId, salary, hra, da, designation)
        {
            this._hours = hour;
            this._rate = rate;
        }

        public int rate
        {
            get { return _rate; }
            set { _rate = value; }
        }

        public int hours
        {
            get { return _hours; }
            set { _hours = value; }
        }

        public override string ToString()
        {
            return base.ToString() + "\n Hour :" + this.hours.ToString() 
                + "\n rate is " + this.rate.ToString();
        }
        public override void AcceptDetails()
        {
            base.AcceptDetails();
            Console.WriteLine(" enter Hour");
            this.hours = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(" enter Rate");
            this.rate = Convert.ToInt32(Console.ReadLine());
        }
        public override void PrintDetails()
        {
            base.PrintDetails();
            Console.WriteLine(this.rate);
        }

        public override double NetSalary()
        {
            return this.rate * this.hours;
        }

    }
    public class Department 
    {
       
        private int _deptNo;
        private string _deptName;
        private string _location;


        public Department()
        {
            this._deptNo = 0;
            this._deptName = "";
                this._location = "";
        }

        public Department(



            double salary,
             int deptNo,
            string deptName, string location)
            


          
        {
            this._deptNo = deptNo;
            this._deptName = deptName;
            this._location = location;
        }



        public string location
        {
            get { return _location; }
            set { _location = value; }
        }


        public string DeptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }


        public int deptNO
        {
            get { return _deptNo; }
            set { _deptNo = value; }
        }

        public override string ToString()
        {
          return "\n DeptNO :" + this._deptNo.ToString()
                + "\n deptName : " + this._deptName.ToString() + "\n location :" + this._location.ToString();
        }
        public void AcceptDetails()
        {
            
            Console.WriteLine(" enter deptno");
            this._deptNo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(" enter dept name");
            this._deptName = Convert.ToString(Console.ReadLine());
            Console.WriteLine(" enter  location");
            this._location = Convert.ToString(Console.ReadLine());
        }
        public  void PrintDetails()
        {
            
            Console.WriteLine(this._deptNo);
            Console.WriteLine(this._deptName);
            Console.WriteLine(this._location);
        }

    }

    
   
}
